import React from 'react';
import './button.css';
import { HashLink } from 'react-router-hash-link';

function PlainButton(props) {
    return (
        <button
            className={`button-${props.style}`}
            onClick={props.onClick}
            type={props.type ? props.type : 'button'}
        >
            {props.text}
        </button>
    );
}

export class Button extends React.Component {
    render() {
        const button = <PlainButton {...this.props} />;
        if (this.props.path) {
            return (
                <HashLink smooth to={this.props.path}>
                    {button}
                </HashLink>
            );
        }
        return button;
    }
}

// forward Ref
