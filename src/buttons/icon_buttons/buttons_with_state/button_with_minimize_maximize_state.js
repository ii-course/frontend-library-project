import React from 'react';
import { faBars, faMinus } from '@fortawesome/free-solid-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export class IconButtonWithMinimizeMaximizeState extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isMinimized: this.props.isMinimized };
        this.icons = {
            minimized: faBars,
            maximized: faMinus,
        };
        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        const { isMinimized } = this.state;
        this.setState(isMinimized ? { isMinimized: false } : { isMinimized: true });
        if (this.state.isMinimized) {
            // this.props.onClick.becameMinimized(); @TODO
        } else {
            // this.props.onClick.becameMaximized(); @TODO
        }
    }

    render() {
        return (
            <button
                className="icon-button"
                onClick={() => {
                    this.props.onClick();
                    this.onClick();
                }}
            >
                <FontAwesomeIcon icon={this.state.isMinimized ? this.icons.minimized : this.icons.maximized} />
            </button>
        );
    }
}
