import { faHandPaper as filledFaHand } from '@fortawesome/free-solid-svg-icons';
import { faHandPaper as unfilledFaHand } from '@fortawesome/free-regular-svg-icons';
import React from 'react';
import {connect, useDispatch} from 'react-redux';
import IconButtonWithFillState from './button_with_fill_state';
import { addBorrowedBook, removeBorrowedBook } from '../../../../store/store/user/user_slice';
import { BorrowBookPopup } from '../../../../popups/borrow_book_popup/borrow_book_popup';
import IconButtonStatic from './static_button';
import {mapStateToProps} from "../../../../store/store/map_state_to_props";

function isEbook(book) {
    return book.details.format === 'E-book';
}

function BorrowButton(props) {
    const dispatch = useDispatch();
    const becameFilled = () => dispatch(addBorrowedBook(props.bookId));
    const becameUnfilled = () => dispatch(removeBorrowedBook(props.bookId));
    const icons = {
        filled: filledFaHand,
        unfilled: unfilledFaHand,
    };
    if (props.user && props.isFilled || !props.book.details.available) {
        return (
            <IconButtonWithFillState
                bookId={props.bookId}
                color={props.color}
                isFilled={props.isFilled}
                becameFilled={becameFilled}
                becameUnfilled={becameUnfilled}
                disabled
                icons={icons}
            />
        );
    }
    if (isEbook(props.book)) {
        return (
            <IconButtonWithFillState
                isFilled={props.isFilled}
                becameFilled={becameFilled}
                becameUnfilled={becameUnfilled}
                bookId={props.bookId}
                color={props.color} icons={icons}
            />
        );
    }
    const button = <IconButtonStatic color={props.color} icons={icons} book={props.book} />;
    return <BorrowBookPopup bookId={props.bookId} trigger={button} />;
}

export default connect(mapStateToProps)(BorrowButton);
