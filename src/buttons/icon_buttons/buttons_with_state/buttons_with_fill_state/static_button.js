import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import '../../icon_button.css';
import {connect} from "react-redux";
import {mapStateToProps} from "../../../../store/store/map_state_to_props";

// eslint-disable-next-line react/display-name
const IconButtonStatic = React.forwardRef(
    ({ ...props }, ref) => {
        return (<button
            ref={ref} {...props}
            className="icon-button"
            disabled={!props.user || !props.book.details.available}
        >
            <FontAwesomeIcon
                icon={props.icons.unfilled}
                color={props.colour}
            />
        </button>);
    }
);

export default connect(mapStateToProps)(IconButtonStatic);
