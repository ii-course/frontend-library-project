import { faBookmark as filledFaBookmark } from '@fortawesome/free-solid-svg-icons';
import { faBookmark as unfilledFaBookmark } from '@fortawesome/free-regular-svg-icons';
import React from 'react';
import { useDispatch } from 'react-redux';
import IconButtonWithFillState from './button_with_fill_state';
import { addSubscribedBook, removeSubscribedBook,
} from '../../../../store/store/user/user_slice';

export function SubscribeButton(props) {
    const icons = {
        filled: filledFaBookmark,
        unfilled: unfilledFaBookmark,
    };
    const dispatch = useDispatch();
    const becameFilled = () => dispatch(addSubscribedBook(props.bookId));
    const becameUnfilled = () => dispatch(removeSubscribedBook(props.bookId));
    return (
        <IconButtonWithFillState
            bookId={props.bookId}
            isFilled={props.isFilled}
            becameFilled={becameFilled}
            becameUnfilled={becameUnfilled}
            color={props.color} icons={icons}
        />
    );
}
