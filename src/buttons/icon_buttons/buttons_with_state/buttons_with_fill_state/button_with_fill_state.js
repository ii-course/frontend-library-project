import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import '../../icon_button.css';
import {connect} from "react-redux";
import {mapStateToProps} from "../../../../store/store/map_state_to_props";

const DISABLED_COLOUR = '#999999';

function IconButtonWithFillState(props) {
    // eslint-disable-next-line no-unused-vars
    const [isFilled, setFilled] = useState(props.isFilled);

    const isDisabled = props.disabled ? props.disabled : !props.user;

    const toggleFilling = () => {
        setFilled((prevFill) => {
            const filledNow = !prevFill;
            if (filledNow) {
                props.becameFilled();
            } else {
                props.becameUnfilled();
            }
            return filledNow;
        });
    };
    return (
        <button
            className="icon-button"
            onClick={toggleFilling}
            disabled={isDisabled}
        >
            <FontAwesomeIcon
                icon={props.isFilled ? props.icons.filled : props.icons.unfilled}
                color={isDisabled ? DISABLED_COLOUR : props.colour}
            />
        </button>
    );
}

export default connect(mapStateToProps)(IconButtonWithFillState);
