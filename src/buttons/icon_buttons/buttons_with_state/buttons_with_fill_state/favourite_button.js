import { faHeart as filledFaHeart } from '@fortawesome/free-solid-svg-icons';
import { faHeart as unfilledFaHeart } from '@fortawesome/free-regular-svg-icons';
import React from 'react';
import { useDispatch } from 'react-redux';
import { addFavouriteBook, removeFavouriteBook } from '../../../../store/store/user/user_slice';
import IconButtonWithFillState from './button_with_fill_state';

export function FavouriteButton(props) {
    const icons = {
        filled: filledFaHeart,
        unfilled: unfilledFaHeart,
    };
    const dispatch = useDispatch();
    const becameFilled = () => { dispatch(addFavouriteBook(props.bookId)); };
    const becameUnfilled = () => { dispatch(removeFavouriteBook(props.bookId)); };
    return (
        <IconButtonWithFillState
            color={props.color}
            isFilled={props.isFilled}
            bookId={props.bookId}
            icons={icons}
            becameFilled={becameFilled}
            becameUnfilled={becameUnfilled}
        />
    );
}
