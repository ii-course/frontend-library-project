import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import '../icon_button.css';
import { HashLink } from 'react-router-hash-link';

export function SearchIconButton(props) {
    return (
        <button className="icon-button" type={props.type ? props.type : 'button'}>
            <FontAwesomeIcon icon={faSearch} />
        </button>
    );
}

export function SearchIconButtonWithPath(props) {
    const button = <SearchIconButton type={props.type} />;
    if (props.path) {
        return (
            <HashLink smooth to={props.path}>
                {button}
            </HashLink>
        );
    }
    return button;
}
