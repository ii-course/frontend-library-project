import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faTimes } from '@fortawesome/free-solid-svg-icons';

import React from 'react';
import '../icon_button.css';
import './close_button.css';
import '../../../scss/icons.css';

const DISABLED_COLOUR = '#999999';

export function CloseButton(props) {
    return (
        <button className="icon-button" onClick={props.onClick} disabled={props.disabled}>
            <FontAwesomeIcon
                icon={faTimes}
                className="close-icon"
                color={props.disabled ? DISABLED_COLOUR : props.color}
            />
        </button>
    );
}
