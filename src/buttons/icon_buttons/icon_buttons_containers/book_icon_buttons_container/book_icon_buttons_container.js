import React, { useEffect, useState } from 'react';
import { FavouriteButton } from '../../buttons_with_state/buttons_with_fill_state/favourite_button';
import { SubscribeButton } from '../../buttons_with_state/buttons_with_fill_state/subscribe_button';
import BorrowButton from '../../buttons_with_state/buttons_with_fill_state/borrow_button';
import {connect} from "react-redux";
import {mapStateToProps} from "../../../../store/store/map_state_to_props";
import {isFavourite} from "../../../../store/repositories/current_user_repository/books/favourite_books";
import {isBorrowed} from "../../../../store/repositories/current_user_repository/books/borrowed_books";
import {isOrdered} from "../../../../store/repositories/current_user_repository/books/ordered_books";
import {isInSubscribed} from "../../../../store/repositories/current_user_repository/books/subscribed_books";
import {isUserLoggedIn} from "../../../../store/repositories/current_user_repository/general";

export function getCurrentButtonsState(bookId) {
    if (isUserLoggedIn()) {
        return {
            favourite: isFavourite(bookId),
            subscribed: isInSubscribed(bookId),
            borrowed: isBorrowed(bookId) || isOrdered(bookId),
        };
    }
    return {
        favourite: false,
        subscribed: false,
        borrowed: false,
    };
}

function BookIconButtonsContainer(props) {
    const colour = props.colour ? props.colour : 'white';
    const [buttonsState, setButtonsState] = useState(getCurrentButtonsState(props.book.id));

    useEffect(() => {
        setButtonsState(getCurrentButtonsState(props.book.id));
    }, [props.user]);

    return (
        <div className="book-icons-container">
            <FavouriteButton
                bookId={props.book.id} colour={colour}
                isFilled={buttonsState.favourite}
            />
            <SubscribeButton
                bookId={props.book.id} colour={colour}
                isFilled={buttonsState.subscribed}
            />
            <BorrowButton
                bookId={props.book.id} colour={colour}
                book={props.book}
                isFilled={buttonsState.borrowed}
            />
        </div>
    );
}

export default connect(mapStateToProps)(BookIconButtonsContainer);
