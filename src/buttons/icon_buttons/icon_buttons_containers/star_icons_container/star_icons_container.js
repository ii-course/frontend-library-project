import { faStar as filledFaStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as unfilledFaStar } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import './star_icons_container.css';

export function StarIconsContainer(props) {
    const DEFAULT_NUMBER_OF_STARS = 5;
    const starsTotal = props.starsTotal ? props.starsTotal : DEFAULT_NUMBER_OF_STARS;
    const starsElements = [];
    for (let i = 0; i < starsTotal; i += 1) {
        let icon;
        if (i < props.starsFilled) {
            icon = <FontAwesomeIcon icon={filledFaStar} />;
        } else {
            icon = <FontAwesomeIcon icon={unfilledFaStar} />;
        }
        starsElements.push(<div key={i.toString()} className="star-icon-wrapper">{icon}</div>);
    }
    return (
        <div className="star-icons-container">
            {starsElements}
        </div>
    );
}
