import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';

import React from 'react';
import './arrow_button.css';
import '../icon_button.css';
import '../../../scss/icons.css';
import { HashLink } from 'react-router-hash-link';

const DISABLED_COLOUR = '#999999';

export function ArrowButtonLeft(props) {
    return (
        <button className="icon-button" onClick={props.onClick} disabled={props.disabled}>
            <FontAwesomeIcon
                icon={faChevronLeft}
                className="arrowIcon"
                color={props.disabled ? DISABLED_COLOUR : props.color}
            />
        </button>
    );
}

export function ArrowButtonRight(props) {
    return (
        <button className="icon-button" onClick={props.onClick} disabled={props.disabled}>
            <FontAwesomeIcon
                icon={faChevronRight}
                className="arrowIcon"
                color={props.disabled ? DISABLED_COLOUR : props.color}
            />
        </button>
    );
}

export function ArrowButtonLeftWithPath(props) {
    const button = <ArrowButtonLeft />;
    return (
        <HashLink smooth to={props.path}>
            {button}
        </HashLink>
    );
}

export function ArrowButtonRightWithPath(props) {
    const button = <ArrowButtonRight />;
    return (
        <HashLink smooth to={props.path}>
            {button}
        </HashLink>
    );
}
