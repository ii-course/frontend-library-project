import React from 'react';
import { Button } from '../../button/button';

export function SignUpButton(props) {
    const style = props.style ? props.style : 'filled';
    const text = props.text ? props.text : 'Sign up';
    const type = props.type ? props.type : 'button';
    return (
        <Button
            style={style}
            text={text}
            type={type}
            onClick={props.onClick}
            path="/sign_up"
        />
    );
}
