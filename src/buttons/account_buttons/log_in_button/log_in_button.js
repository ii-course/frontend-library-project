import React from 'react';
import { Button } from '../../button/button';

export function LogInButton(props) {
    const style = props.style ? props.style : 'filled';
    const text = props.text ? props.text : 'Log In';
    const type = props.type ? props.type : 'button';
    return (
        <Button
            style={style}
            text={text}
            type={type}
        />
    );
}
