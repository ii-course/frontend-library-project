import React from 'react';
import { useDispatch } from 'react-redux';
import { Button } from '../../button/button';
import { logOut } from '../../../store/store/user/user_slice';

export function LogOutButton() {
    const dispatch = useDispatch();
    return (
        <Button
            style="unfilled"
            text="Log Out"
            onClick={() => dispatch(logOut())}
            path="/"
        />
    );
}
