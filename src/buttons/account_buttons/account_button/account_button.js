import React from 'react';
import { Button } from '../../button/button';

export function AccountButton() {
    return (
        <Button
            style="filled"
            text="Account"
            path="/account"
        />
    );
}
