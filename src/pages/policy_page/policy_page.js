import React from 'react';
import '../../scss/wrapper.css';
import '../../buttons/button/button.css';
import './policy_page.css';
import { getTableOfContents, getIdForHeading } from '../table_of_contents';

function getSections(policy) {
    const sectionsElements = [];
    policy.forEach((section) => {
        const id = getIdForHeading(section.heading);
        const element = (
            <section id={id} key={id}>
                <h2>{section.heading}</h2>
                <p>{section.text}</p>
            </section>
        );
        sectionsElements.push(element);
    });
    return sectionsElements;
}

export function PolicyPage(props) {
    return (
        <main className="content-wrapper policy-page larger-top-margin">
            <h1>{props.heading}</h1>
            {getTableOfContents(props.policy)}
            {getSections(props.policy)}
        </main>
    );
}
