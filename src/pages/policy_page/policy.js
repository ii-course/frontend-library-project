export const policy = [
    {
        heading: 'Lorem ipsum',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus rutrum nec nisi in sodales. Vestibulum augue augue, fringilla eu nibh eget, vulputate volutpat sem. Nunc ac quam vitae quam varius ultricies. Phasellus vel eros vel nunc egestas porttitor at non dolor. Aliquam sit amet ipsum vel tortor suscipit convallis. Donec eu fringilla magna, ut varius lectus. Nulla facilisi. In rutrum vulputate nisl, sit amet vulputate nibh. Sed enim odio, pulvinar nec dui sit amet, bibendum ornare sapien. Nunc semper risus a nisl aliquam, ac porttitor ligula finibus. Mauris nulla ipsum, egestas a feugiat et, pellentesque vel justo. Pellentesque vehicula neque eget dui volutpat, faucibus molestie eros ultricies. Morbi arcu ipsum, ornare et volutpat a, vehicula a libero. In suscipit justo sit amet semper pulvinar. Proin finibus fringilla nisl, nec cursus nibh placerat et. Maecenas tincidunt lectus quis ipsum semper, a rutrum dolor varius. ',
    },
    {
        heading: 'Maecenas ultricies',
        text: 'Maecenas ultricies diam non tellus vehicula, vitae tristique mauris varius. Praesent nunc quam, faucibus pellentesque libero sit amet, gravida cursus augue. Nullam hendrerit vulputate quam. Duis commodo justo a sollicitudin viverra. Donec dignissim ligula eros, id bibendum nibh facilisis vitae. Nam consequat orci lacus, vitae dignissim risus malesuada id. Duis blandit suscipit eros ac sollicitudin. Mauris quis felis a turpis placerat ultricies. Ut efficitur semper est, et porta urna lacinia eu. Donec auctor vitae nibh id dignissim. Vivamus vitae sem efficitur, porttitor erat et, elementum eros. Fusce eu malesuada erat. ',
    },
    {
        heading: 'Sed iaculis',
        text: 'Sed iaculis sed augue et consequat. Duis sagittis magna massa, a rhoncus quam rhoncus sit amet. Suspendisse aliquet neque porta ipsum vehicula imperdiet nec ac leo. Curabitur rutrum nisl leo, vel luctus nisl dictum eu. Cras tincidunt, orci at scelerisque sodales, mi massa mattis risus, in vestibulum eros nisi non purus. Nullam vulputate tincidunt libero tristique ultrices. Etiam mi lectus, ultrices nec est quis, malesuada vestibulum ipsum. Maecenas ultrices dui sed nunc laoreet, sed congue erat dapibus. Donec rhoncus augue eget viverra ornare. Sed eu tempus ante, sit amet tristique urna. ',
    },
    {
        heading: 'Etiam laoreet',
        text: 'Etiam laoreet tincidunt mi ut volutpat. In ut fermentum erat, ac placerat libero. Etiam et leo hendrerit, aliquam felis nec, aliquet urna. Aliquam id urna augue. Cras eget cursus ex. Phasellus tincidunt ligula vitae lorem elementum, id hendrerit tortor varius. Phasellus sed lacus ac odio dignissim feugiat at et eros. Proin viverra fringilla purus sed molestie. Quisque consectetur viverra sapien egestas semper. Fusce elementum sed lorem quis pulvinar. Curabitur auctor, metus non molestie sagittis, dui tellus elementum ante, accumsan convallis lectus metus sit amet neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
    },
    {
        heading: 'Nulla sagittis',
        text: 'Nulla sagittis non mi eu blandit. Nullam vulputate, dui ac sagittis posuere, ex odio efficitur nisi, ac dignissim quam massa at eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ullamcorper nunc vitae lacinia viverra. Sed ullamcorper purus eu luctus dictum. Suspendisse dapibus, metus at ultrices vulputate, nibh lectus interdum orci, ac ornare sapien mauris a felis. Praesent interdum dui diam, sed euismod dui ultricies eu.\n',
    },
];
