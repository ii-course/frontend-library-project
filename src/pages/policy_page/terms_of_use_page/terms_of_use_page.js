import { PolicyPage } from '../policy_page';
import { policy } from '../policy';
import React from "react";

export function TermsOfUsePage() {
    return <PolicyPage heading="Terms of use" policy={policy} />;
}
