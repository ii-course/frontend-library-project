import React from 'react';
import { PolicyPage } from '../policy_page';
import { policy } from '../policy';

export function PrivacyPolicyPage() {
    return <PolicyPage heading="Privacy Policy" policy={policy} />;
}
