import React, { useEffect, useState } from 'react';
import '../../scss/wrapper.css';
import './author_page.css';
import { useParams } from 'react-router-dom';
import { getAuthorById } from '../../store/repositories/authors_repository';
import { AuthorPortfolio } from './author_portfolio/author_portfolio';

export function AuthorPage() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [author, setAuthor] = useState([]);
    const { id } = useParams();
    useEffect(() => {
        getAuthorById(id)
            .then(
                (authorFetched) => {
                    setAuthor(authorFetched);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [id]);

    if (error) {
        return (
            <div>
              Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <AuthorPortfolio author={author} />;
}
