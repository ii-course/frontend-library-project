import React from 'react';
import RecentlySeenBooksGallery from '../../../galleries/books_galleries/books_scroll_gallery/recently_seen_books_gallery/recently_seen_books_gallery';
import { getFullName } from '../../../helpers/strings';
import { AuthorBooksGalleryWithPagination } from '../../../galleries/books_galleries/books_gallery_with_pagination/author_books_gallery_with_pagination/author_books_gallery_with_pagination';

function getFullYearFormatted(dateString) {
    if (dateString) {
        return new Date(dateString).getFullYear();
    }
    return '...';
}

function YearsOfLife(props) {
    return (
        <p className="caption-text">
            {`(${getFullYearFormatted(props.birthdate)}–${getFullYearFormatted(props.deathDate)})`}
        </p>
    );
}

function ImgWithCaption(props) {
    return (
        <div className="img-with-alt-container">
            <div className="img-container">
                <img src={props.img.src} alt={props.img.alt} />
            </div>
            <p className="caption-text">{props.img.alt}</p>
        </div>
    );
}

export function AuthorPortfolio(props) {
    return (
        <main className="content-wrapper author-page larger-top-margin">
            <div className="author-info-container">
                <h1>{getFullName(props.author.name)}</h1>
                <YearsOfLife
                    birthdate={props.author.birthdate}
                    deathDate={props.author.deathDate}
                />
                <p className="caption-text quote">{props.author.quote}</p>
                <div>
                    <ImgWithCaption img={props.author.img} />
                    <p className="bio">{props.author.biographyFull}</p>
                </div>
            </div>
            <h2>Books</h2>
            <AuthorBooksGalleryWithPagination authorId={props.author.id} />
            <h2>Recently seen</h2>
            <RecentlySeenBooksGallery />
        </main>
    );
}

// @TODO: routing recently seen
