import React from 'react';
import {
    MapContainer, TileLayer, Marker,
} from 'react-leaflet';
import './map.css';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';

import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';

const DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: iconShadow,
});

L.Marker.prototype.options.icon = DefaultIcon;

export function Map() {
    const position = [51.505, -0.09];
    return (
        <div id="map" className="map">
            <MapContainer
                center={position} zoom={13} scrollWheelZoom style={{
                    height: '100%',
                }}
            >
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={position} />
            </MapContainer>
        </div>
    );
}

// @TODO: marker not displayed
