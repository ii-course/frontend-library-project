import React from 'react';
import './about_us_page.css';
import '../../scss/wrapper.css';
import {Button} from '../../buttons/button/button';
import {Map} from './map/map';
import {FAQ, HISTORY_TEXT} from "./data";

function getFAQ() {
    const elements = [];
    FAQ.forEach((questionWithAnswer) => {
        const element = (
            <details>
                <summary>
                    {questionWithAnswer.question}
                </summary>
                {questionWithAnswer.answer}
            </details>
        );
        elements.push(element);
    });
    return elements;
}

export function AboutUsPage() {
    return (
        <main className="about-us-page larger-top-margin">
            <section>
                <h1 className="first-heading" id="history">HISTORY</h1>
                <img
                    className="circle-img"
                    src="https://images.unsplash.com/photo-1572582110235-d3a25d641b8e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=889&q=80"
                    alt="The New York Public Library"
                />
                <p className="content-wrapper two-columns">{HISTORY_TEXT}</p>
                <img
                    className="full-screen-img"
                    src="https://images.unsplash.com/photo-1505664194779-8beaceb93744?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1050&q=80"
                    alt="Book lot on black wooden shelf"
                />
            </section>
            <section className="content-wrapper faq-section">
                <h1 id="faq">FAQ</h1>
                {getFAQ()}
            </section>
            <section className="content-wrapper">
                <h1>
              TERMS OF USE &
              PRIVACY POLICY
                </h1>
                <p>Pontus instabilis nondum poena videre ensis aeris dixere metusque stagna sine dei ventis usu quarum mundo gentes nullus carentem deducite retinebat litem caligine agitabilis frigida homini quae sidera terris tollere hunc liquidas iapeto deducite quem magni caelumque cuncta congeriem effervescere turba dissociata coegit cepit liquidum igni fuerant di pace habitabilis militis homo sata congestaque dei fossae mundi aequalis nec, unus nondum lumina reparabat dixere cingebant.</p>
                <Button text="TERMS OF USE" style="dark-link" path="/terms_of_use" />
                <Button text="PRIVACY POLICY" style="dark-link" path="/privacy_policy" />
            </section>
            <section className="content-wrapper contacts-section">
                <h1>CONTACTS</h1>
                <div className="contacts-container">
                    <p>+123 45 67</p>
                    <p>library@example.com</p>
                    <p>6 Southwark St, London</p>
                </div>
                <div id="map" className="map"><Map /></div>
            </section>
        </main>
    );
}
