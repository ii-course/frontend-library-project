import React from "react";
import {SignUpPage} from "./sign_up_page/sign_up_page";
import  { Redirect } from 'react-router-dom';
import {connect} from "react-redux";
import {mapStateToProps} from "../../store/store/map_state_to_props";

function SignUpPageWrapper(props) {
    return props.user ? <Redirect to='/account'  /> : <SignUpPage />;
}

export default connect(mapStateToProps)(SignUpPageWrapper);
