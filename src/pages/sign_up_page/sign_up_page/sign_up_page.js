import React from 'react';
import '../../../scss/wrapper.css';
import './sign_up_page.css';
import {SignUpFormWithFormik} from "../../../forms/forms/sign_up_form/sign_up_form_with_formik";

export function SignUpPage() {
    return (
        <main className="content-wrapper sign-up-page larger-top-margin">
            <h1>Welcome to the library</h1>
            <SignUpFormWithFormik />
        </main>
    );
}
