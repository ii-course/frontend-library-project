import React from 'react';
import { ArticlesSection } from '../home_page/articles_section/articles_section';
import './whats_on_page.css';

export function WhatsOnPage() {
    return (
        <main className="whats-on-page larger-top-margin">
            <ArticlesSection />
        </main>
    );
}
