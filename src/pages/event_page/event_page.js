import React, { useState, useEffect } from 'react';
import '../../scss/wrapper.css';
import './event_page.css';
import { useParams } from 'react-router-dom';
import { getEventById } from '../../store/repositories/events_repository';

function getHeadingsWithText(fieldsWithHeadings, event) {
    const headingsWithText = [];
    fieldsWithHeadings.forEach((fieldsWithHeading) => {
        const headingWithText = (
            <>
                <h2>{fieldsWithHeading.heading}</h2>
                <p>{event[fieldsWithHeading.field]}</p>
            </>
        );
        headingsWithText.push(headingWithText);
    });
    return headingsWithText;
}

function EventContainer(props) {
    const fieldsWithHeadings = [
        {
            field: 'when',
            heading: 'When',
        },
        {
            field: 'where',
            heading: 'Where',
        },
        {
            field: 'participation',
            heading: 'How to participate',
        },
    ];
    return (
        <main className="content-wrapper event-page larger-top-margin">
            <h1>{props.event.heading}</h1>
            {getHeadingsWithText(fieldsWithHeadings, props.event)}
            <img src={props.event.img.src} alt={props.event.img.alt} />
            <p className="two-columns">{props.event.description}</p>
        </main>
    );
}

export function EventPage() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [event, setEvent] = useState({});
    const { id, type } = useParams();

    useEffect(() => {
        getEventById(type, id)
            .then(
                (eventFetched) => {
                    setEvent(eventFetched);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [id, type]);

    if (error) {
        return (
            <div>
              Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <EventContainer event={event} />;
}
