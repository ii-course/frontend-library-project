import React, { useState } from 'react';
import '../../../scss/wrapper.css';
import './catalogue_browser.css';
import qs from 'query-string';
import {
    useLocation, useHistory,
} from 'react-router-dom';
import { IconButtonWithMinimizeMaximizeState } from '../../../buttons/icon_buttons/buttons_with_state/button_with_minimize_maximize_state';
import RecentlySeenBooksGallery from '../../../galleries/books_galleries/books_scroll_gallery/recently_seen_books_gallery/recently_seen_books_gallery';
import { AuthorsGalleryWithFetching } from '../../../galleries/authors_galleries/authors_gallery_wrapper/authors_gallery_wrapper';
import { BooksGalleryWithPaginationWithFetchByParams } from '../../../galleries/books_galleries/books_gallery_with_pagination/books_gallery_with_pagination_with_fetch_by_params/books_gallery_with_pagination_with_fetch_by_params';
import { SearchFormWithFormik } from '../../../forms/forms/search_form/search_form_with_formik';
import { SortOptionsFormWithFormik } from '../../../forms/forms/sort_options_form/sort_options_form_with_formik';
import {FiltersFormWithFormik} from "../../../forms/forms/filters_form/filters_form_with_formik";
import './../../../forms/forms/sort_options_form/sort_options_form.css';
import './../../../forms/forms/form_with_checkboxes.css';

function useQuery() {
    return new qs.parse(useLocation().search);
}

export function CatalogueBrowser() {
    const [filtersOpened, setFiltersOpened] = useState(false);

    const query = useQuery();
    const history = useHistory();

    const toggleState = () => {
        setFiltersOpened((wereFiltersOpened) => !wereFiltersOpened);
    };

    const filters = <div className="filter-form form-with-checkboxes">
        <h2>Filters</h2>
        <FiltersFormWithFormik history={history} />
    </div>;

    return (
        <main className="catalogue-browser content-wrapper larger-top-margin">
            <SearchFormWithFormik history={history} />
            <IconButtonWithMinimizeMaximizeState isMinimized onClick={toggleState} />
            <div className="catalogue-browser-content-container">
                <div className="catalogue-browser-main-container  content-wrapper" key="catalogueBrowserMainContainer">
                    {filtersOpened ? filters : <></>}
                    <div className="catalogue-browser-gallery-container">
                        <h1>Catalogue</h1>
                        <section>
                            <h2>Authors</h2>
                            <AuthorsGalleryWithFetching booksParams={query} />
                        </section>
                        <section>
                            <h2>Books</h2>
                            <SortOptionsFormWithFormik />
                            <BooksGalleryWithPaginationWithFetchByParams params={query} />
                        </section>
                    </div>
                </div>
                <div key="recentlySeen" className="recently-seen-gallery-container">
                    <h3>Recently seen</h3>
                    <RecentlySeenBooksGallery />
                </div>
            </div>
        </main>
    );
}
