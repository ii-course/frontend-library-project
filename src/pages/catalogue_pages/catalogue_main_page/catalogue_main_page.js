import React, { useState, useEffect } from 'react';
import '../../../scss/wrapper.css';
import { Button } from '../../../buttons/button/button';
import './catalogue_main_page.css';
import { capitalize } from '../../../helpers/strings';
import { getBooksByGenreLimit} from '../../../store/repositories/books_repository';
import { SearchFormWithFormik } from '../../../forms/forms/search_form/search_form_with_formik';
import {BooksScrollGallery} from "../../../galleries/books_galleries/books_scroll_gallery/books_scroll_gallery";

const listOfGenres = {
    fiction: [
        'Action and adventure',
        'Children\'s',
        'Classic',
        'Crime',
        'Drama',
        'Fantasy',
        'Folklore',
        'Historical',
        'Horror',
        'Mystery',
        'Romance',
        'Science fiction',
        'Thriller',
        'Young adult',
        'Poetry',
    ],
    nonFiction: [
        'Popular Science',
        'History',
        'Biographies',
        'Travelling',
        'Academic texts',
        'Philosophy',
        'Journalism',
        'Self-help',
        'Guides and manuals',
        'Humor',
    ],
};

function GenreSection(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [books, setBooks] = useState([]);

    useEffect(() => {
        getBooksByGenreLimit(props.genre)
            .then(
                (result) => {
                    setBooks(result);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.genre]);
    if (error) {
        return (
            <div>
              Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return (
        <section className="content-wrapper">
            <h3>{capitalize(props.genre)}</h3>
            <BooksScrollGallery books={books} />
        </section>
    );
}

function getAllSectionsForType(type) {
    const sections = [];
    listOfGenres[type].forEach((genre) => {
        const section = <GenreSection genre={genre} />;
        sections.push(section);
    });
    return sections;
}

function SectionsContainer(props) {
    return (
        <section>
            <h2 className="content-wrapper" id={props.type}>{capitalize(props.heading)}</h2>
            {getAllSectionsForType(props.type)}
        </section>
    );
}

export function CatalogueMainPage() {
    return (
        <main className="catalogue-main-page larger-top-margin">
            <div className="content-wrapper catalogue-main-page-header">
                <h1 className="first-heading">Catalogue</h1>
                <p>
                Securae ambitae montes liquidum terras quam quanto moderantum
                subdita erant alto circumfluus imagine tumescere sidera sine nuper
                freta caelo adhuc ille erat: tonitrua os gentes quod non est aeris ventos metusque
                </p>
                <SearchFormWithFormik />
                <Button style="filled" path="/catalogue_browser" text="Browse all" />
            </div>
            <SectionsContainer heading="Fiction" type="fiction" />
            <SectionsContainer heading="Non-fiction" type="nonFiction" />
        </main>
    );
}
