import React from 'react';
import { SignUpButton } from '../../../buttons/account_buttons/sign_up_button/sign_up_button';
import { FilledContainerWithButton } from '../../../containers/filled_container_with_button/container';
import {connect} from "react-redux";
import {mapStateToProps} from "../../../store/store/map_state_to_props";

function JoinUsCard(props) {
    return props.user ? <></> : (
        <FilledContainerWithButton
            heading="Explore the literature world"
            text={`Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                           sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`}
            button={<SignUpButton text="Join us" />}
        />
    );
}

export default connect(mapStateToProps)(JoinUsCard);
