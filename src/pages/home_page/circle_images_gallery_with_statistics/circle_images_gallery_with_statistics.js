import React, { useState, useEffect } from 'react';
import { CircleImagesStaticGallery } from '../../../galleries/circle_images_static_gallery/gallery';
import { getAllBooksCount } from '../../../store/repositories/books_repository';
import { getAllAuthorsCount } from '../../../store/repositories/authors_repository';
import { getAllUsersCount } from '../../../store/repositories/users_repository';

function getBooksCardData() {
    const data = {
        img: {
            src: 'https://www.charlesdickensinfo.com/wp-content/uploads/2012/07/pileOldBooks.png',
            alt: '',
        },
        heading: '0',
        text: 'books',
    };
    return getAllBooksCount()
        .then(
            (count) => {
                data.heading = count.toString();
                return data;
            },
            (error) => {
                console.error(error);
                return data;
            },
        );
}

function getAuthorsCardData() {
    const data = {
        img: {
            src: 'https://images.theconversation.com/files/329522/original/file-20200421-82672-10gj7rb.jpg?ixlib=rb-1.1.0&rect=0%2C15%2C2600%2C2576&q=45&auto=format&w=926&fit=clip',
            alt: '',
        },
        heading: '0',
        text: 'authors',
    };
    return getAllAuthorsCount()
        .then(
            (count) => {
                data.heading = count.toString();
                return data;
            },
            (error) => {
                console.error(error);
                return data;
            },
        );
}

function getUsersCardData() {
    const data = {
        img: {
            src: 'https://ebookfriendly.com/wp-content/uploads/2013/06/The-Aldine.jpg',
            alt: '',
        },
        heading: '0',
        text: 'readers',
    };
    return getAllUsersCount()
        .then(
            (count) => {
                data.heading = count.toString();
                return data;
            },
            (error) => {
                console.error(error);
                return data;
            },
        );
}

export function CircleImagesGalleryWithStatistics() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [booksData, setBooksData] = useState({});
    const [authorsData, setAuthorsData] = useState({});
    const [usersData, setUsersData] = useState({});

    useEffect(() => {
        getBooksCardData()
            .then(
                (booksDataFetched) => {
                    setBooksData(booksDataFetched);
                    return getAuthorsCardData();
                },
            )
            .then(
                (authorsDataFetched) => {
                    setAuthorsData(authorsDataFetched);
                    return getUsersCardData();
                },
            )
            .then(
                (usersDataFetched) => {
                    setUsersData(usersDataFetched);
                },
            )
            .then(
                () => {
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, []);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <CircleImagesStaticGallery cards={[booksData, authorsData, usersData]} />;
}
