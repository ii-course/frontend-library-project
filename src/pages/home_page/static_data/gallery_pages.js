export const mainGalleryPages = [
    {
        img: {
            src: 'https://images.unsplash.com/photo-1465929639680-64ee080eb3ed?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
            alt: '',
        },
        heading: 'Collections',
        text: 'Morbi tristique nibh vel velit lacinia ultricies',
    },
    {
        img: {
            src: 'https://images.unsplash.com/photo-1554907984-15263bfd63bd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1050&q=80',
            alt: '',
        },
        heading: 'Exhibitions',
        text: 'Sed elit arcu, convallis nec suscipit sit amet, semper sit amet urna. Donec mi enim, posuere eget arcu quis, tempus mollis odio.',
    },
    {
        img: {
            src: 'https://images.unsplash.com/photo-1501622549218-2c3ef86627cb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1052&q=80',
            alt: 'Crowd of people',
        },
        heading: 'Community',
        text: 'Cras euismod tellus ac ante aliquam pellentesque',
    },
    {
        img: {
            src: 'https://images.unsplash.com/photo-1580687581486-9d5aa7b902b3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1050&q=80',
            alt: 'Lorem Ipsum',
        },
        heading: 'Membership',
        text: 'Maecenas maximus sit amet arcu vitae consequat',
    },
];
