import React from 'react';
import { SliderGallery } from '../../galleries/gallery/gallery';
import { ArticlesSection } from './articles_section/articles_section';
import { mainGalleryPages } from './static_data/gallery_pages';
import { HistoryCard } from './history_card/history_card';
import JoinUsCard from './join_us_card/join_us_card';
import { CircleImagesGalleryWithStatistics } from './circle_images_gallery_with_statistics/circle_images_gallery_with_statistics';

export function HomePage() {
    return (
        <main>
            <SliderGallery pages={mainGalleryPages} className="gallery-home-page" />
            <CircleImagesGalleryWithStatistics />
            <HistoryCard />
            <JoinUsCard />
            <ArticlesSection />
        </main>
    );
}
