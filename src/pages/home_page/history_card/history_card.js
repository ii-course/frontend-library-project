import React from 'react';
import { FullScreenArticleCard } from '../../../cards/articles_cards/article_cards_with_image/full_screen_article_card/card';

const historyImg = {
    src: 'https://images.unsplash.com/photo-1437751068958-82e6fccc9360?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1050&q=80',
    alt: 'Lorem Ipsum',
};

const historyText = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                   sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                   Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                                   nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                                   reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                                   Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
                                   deserunt mollit anim id est laborum
                                   Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                   sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                   Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                                   nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                                   reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                                   Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
                                   deserunt mollit anim id est laborum`;

export function HistoryCard() {
    return (
        <FullScreenArticleCard
            heading="Our history"
            text={historyText}
            img={historyImg}
            path="/about_us#history"
        />
    );
}
