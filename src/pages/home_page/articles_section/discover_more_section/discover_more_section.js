import React from 'react';
import './discover_more_section.css';
import { BaseSection } from './base_section/base_section';

export function DiscoverMoreSection() {
    return (
        <section className="content-wrapper">
            <h2 id="misc">Discover more</h2>
            <div className="discover-more-home-page-container">
                <BaseSection eventsType="discussions" heading="Talks & discussions" />
                <BaseSection eventsType="tours" />
                <BaseSection eventsType="workshops" />
            </div>
        </section>
    );
}
