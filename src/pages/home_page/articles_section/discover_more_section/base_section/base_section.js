import React, { useState, useEffect } from 'react';
import { getEvents } from '../../../../../store/repositories/events_repository';
import { capitalize } from '../../../../../helpers/strings';
import { getEventsCards } from '../event_cards_formation';

function EventsContainer(props) {
    return (
        <section key={props.heading}>
            <h3 className="discover-more-main-heading">{props.heading}</h3>
            <hr />
            {getEventsCards(props.events, props.eventsType)}
        </section>
    );
}

export function BaseSection(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [events, setEvents] = useState([]);

    useEffect(() => {
        getEvents(props.eventsType.toLowerCase())
            .then(
                (events) => {
                    setEvents(events);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.eventsType]);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    const heading = props.heading ? props.heading : capitalize(props.eventsType);
    return <EventsContainer events={events} eventsType={props.eventsType.toLowerCase()} heading={heading} />;
}
