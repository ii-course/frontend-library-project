import React from 'react';
import { EventTextContainer } from '../../../../cards/events_cards/event_text_container/default_event_text_container/event_text_container';

function getEventCard(event, eventType, button) {
    return (
        <EventTextContainer
            event={event}
            button={button}
            eventType={eventType}
        />
    );
}

export function getEventsCards(events, eventsType, button = { style: 'dark-link' }) {
    const cards = [];
    events.forEach((event) => {
        const card = getEventCard(event, eventsType, button);
        cards.push(card);
    });
    return cards;
}
