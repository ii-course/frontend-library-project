import React, { useState, useEffect } from 'react';
import { getAllArticles } from '../../../../store/repositories/articles_repository';
import { CardWithBackgroundImage } from '../../../../cards/articles_cards/article_cards_with_image/card_with_background_image/card';
import { CardWithTopImage } from '../../../../cards/articles_cards/article_cards_with_image/card_with_top_image/card_with_top_image';
import { ArticleTextContainer } from '../../../../cards/articles_cards/article_text_container/container';
import './news_section.css';

function CardWithTopImageWrapper(props) {
    if (props.article) {
        const button = {
            style: 'dark-link',
            path: `/articles/${props.article.id}`,
        };
        return (
            <CardWithTopImage
                article={props.article}
                button={button}
            />
        );
    }
    return <></>;
}

function CardWithBackgroundImageWrapper(props) {
    if (props.article) {
        const button = {
            style: 'filled',
            path: `/articles/${props.article.id}`,
        };
        return <CardWithBackgroundImage article={props.article} button={button} />;
    }
    return <></>;
}

function ArticleTextContainerWrapper(props) {
    if (props.article) {
        const button = {
            style: 'dark-link',
            path: `/articles/${props.article.id}`,
        };
        return <ArticleTextContainer article={props.article} button={button} />;
    }
    return <></>;
}

function getCardsWithTopImage(articles, startFrom, count = 2) {
    const cards = [];
    for (let i = startFrom; i < articles.length && i < startFrom + count; i += 1) {
        const card = <CardWithTopImageWrapper article={articles[i]} />;
        cards.push(card);
    }
    return cards;
}

function getArticlesTextContainers(articles, startFrom, count = 4) {
    const cards = [];
    for (let i = startFrom; i < articles.length && i < startFrom + count; i += 1) {
        const card = <ArticleTextContainerWrapper article={articles[i]} />;
        cards.push(card);
    }
    return cards;
}

export function NewsSection() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [articles, setArticles] = useState([]);

    useEffect(() => {
        getAllArticles()
            .then(
                (articles) => {
                    setArticles(articles);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, []);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return (
        <section className="content-wrapper">
            <h2 id="news">News</h2>
            <div className="news-with-images-home-page">
                <CardWithBackgroundImageWrapper article={articles[0]} />
                <div className="news-with-images-home-page-cards-with-top-images">
                    {getCardsWithTopImage(articles, 1)}
                </div>
            </div>
            <div className="news-text-cards-home-page">
                {getArticlesTextContainers(articles, 3)}
            </div>
        </section>
    );
}
