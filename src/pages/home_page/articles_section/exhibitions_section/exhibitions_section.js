import React from 'react';
import './exhibitions_section.css';
import {ExhibitionsGalleryWithPagination} from "../../../../galleries/events_galleries/square_images_cards_gallery_with_pagination/exhibitions_gallery_with_pagination";

export function ExhibitionsSection() {
    return (
        <section className="exhibitions-gallery-background-container-home-page">
            <h1 id="events" >Events</h1>
            <h2 id="exhibitions">Exhibitions</h2>
            <div className="content-wrapper">
                <ExhibitionsGalleryWithPagination />
            </div>
        </section>
    );
}
