import React from 'react';
import '../../../scss/wrapper.css';
import './articles_section.css';
import { ExhibitionsSection } from './exhibitions_section/exhibitions_section';
import { DiscoverMoreSection } from './discover_more_section/discover_more_section';
import { NewsSection } from './news_section/news_section';

export function ArticlesSection() {
    return (
        <div className="articles-section">
            <div className="content-wrapper">
                <h1>News & Articles</h1>
            </div>
            {<NewsSection />}
            {<ExhibitionsSection />}
            {<DiscoverMoreSection />}
        </div>
    );
}

// @TODO: addToRecentlySeen gallery with pagination for news
