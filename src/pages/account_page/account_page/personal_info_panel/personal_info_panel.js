import React from 'react';
import './personal_info_panel.css';
import { getFullName } from '../../../../helpers/strings';
import '../../../../containers/dossier_container.css';

function getDateFormatted(dateString) {
    return new Date(dateString).toLocaleDateString('en-GB');
}

export function PersonalInfoPanel(props) {
    const { user } = props;
    const fullName = getFullName(user.name);
    return (
        <div className="personal-info-panel">
            <div className="img-container">
                <img src={user.picture.large} alt={fullName} />
            </div>
            <h1>{fullName}</h1>
            <section>
                <h2>Personal info</h2>
                <div className="dossier-container">
                    <span className="dossier-label">{'Birthdate: '}</span>
                    <span>{getDateFormatted(user.dob.date)}</span>
                </div>
                {user.location.country ?
                    <div className="dossier-container">
                        <span className="dossier-label">{'Country: '}</span>
                        <span>{user.location.country}</span>
                    </div> : <></>}
                {user.location.city ?
                    <div className="dossier-container">
                        <span className="dossier-label">{'Location: '}</span>
                        <span>{user.location.city}</span>
                    </div> : <></> }
            </section>
            <section>
                <h2>Contacts</h2>
                <p>{user.phone}</p>
                <p>{user.email}</p>
            </section>
            <section>
                <h2>Statistics</h2>
                <p>{`${user.booksReadId.length} books read`}</p>
                <p>{`${user.booksBorrowed.length} books currently borrowed`}</p>
                <p>{`${user.booksFavouriteId.length} favourite books`}</p>
            </section>
        </div>
    );
}
