import React from 'react';
import { PersonalInfoPanel } from './personal_info_panel/personal_info_panel';
import '../../../scss/wrapper.css';
import './account_page.css';
import { BooksGroupedByDateGallery } from '../../../galleries/books_galleries/books_grouped_by_date_gallery/books_grouped_by_date_gallery';
import { getTableOfContents } from '../../table_of_contents';
import { BooksGalleryWithFetchById } from '../../../galleries/books_galleries/books_gallery_with_fetch_by_id/books_gallery_with_fetch_by_id';
import { UserReviewsGalleryWithPagination } from '../../../galleries/comments_galleries/comment_gallery_with_pagination/user_reviews_gallery_with_pagination';
import {BooksGalleryWithPaginationWithFetchByParams} from "../../../galleries/books_galleries/books_gallery_with_pagination/books_gallery_with_pagination_with_fetch_by_params/books_gallery_with_pagination_with_fetch_by_params";
import {connect} from "react-redux";
import {mapStateToProps} from "../../../store/store/map_state_to_props";

const sections = [
    {
        heading: 'Currently borrowed',
    },
    {
        heading: 'Ordered',
    },
    {
        heading: 'Favourites',
    },
    {
        heading: 'Subscribed',
    },
    {
        heading: 'Already read',
    },
    {
        heading: 'Reviews',
    },
];

function AccountPage(props) {
    return (
        <main className="content-wrapper account-page larger-top-margin">
            <div className="personal-info-with-borrowed-books">
                <div>
                    <PersonalInfoPanel user={props.user} />
                    {getTableOfContents(sections)}
                </div>
                <section id="currently-borrowed" key="currently-borrowed">
                    <h2 className="currently-borrowed-heading">Currently borrowed</h2>
                    <BooksGroupedByDateGallery
                        books={props.user.booksBorrowed} dateField="endDate"
                        labelBeforeDate="due"
                    />
                </section>
            </div>
            <section id="ordered" key="ordered">
                <h2>Ordered</h2>
                <BooksGroupedByDateGallery
                    books={props.user.booksOrdered} dateField="receivingDate"
                    labelBeforeDate="for"
                />
            </section>
            <section id="favourites" key="favourites">
                <h2>Favourites</h2>
                <BooksGalleryWithFetchById
                    booksId={props.user.booksFavouriteId}
                    galleryType="scroll"
                />
            </section>
            <section id="subscribed" key="subscribed">
                <h2>Subscribed</h2>
                <BooksGalleryWithPaginationWithFetchByParams
                    params={{id: props.user.booksSubscribedId}}
                    emptyOption={true}
                />
            </section>
            <section id="already-read" key="already-read">
                <h2>Already read</h2>
                <BooksGalleryWithPaginationWithFetchByParams
                    params={{id: props.user.booksReadId}}
                    emptyOption={true}
                />
            </section>
            <section id="reviews" key="reviews">
                <h2>Reviews</h2>
                <UserReviewsGalleryWithPagination userId={props.user.id} />
            </section>
        </main>
    );
}

export default connect(mapStateToProps)(AccountPage);

