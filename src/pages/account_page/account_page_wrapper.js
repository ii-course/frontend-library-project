import React from 'react';
import AccountPage from './account_page/account_page';
import {isUserLoggedIn} from "../../store/repositories/current_user_repository/general";

export function AccountPageWrapper() {
    if (isUserLoggedIn()) {
        return <AccountPage />;
    }
    return <main>No user logged in</main>;
}
