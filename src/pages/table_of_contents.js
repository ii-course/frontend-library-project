import { HashLink } from 'react-router-hash-link';
import React from 'react';

export function getIdForHeading(heading) {
    return heading.toLowerCase().split(' ').join('-');
}

export function getTableOfContents(sections) {
    const items = [];
    sections.forEach((section) => {
        const id = getIdForHeading(section.heading);
        const to = `#${id}`;
        const item = (
            <li key={id}>
                <HashLink smooth to={to} className="button-dark-link">{section.heading}</HashLink>
            </li>
        );
        items.push(item);
    });
    return (
        <nav>
            <ul>
                {items}
            </ul>
        </nav>
    );
}
