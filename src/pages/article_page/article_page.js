import './article_page.css';
import '../../scss/wrapper.css';
import { useParams } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import { TextContainerWithBackgroundImage } from '../../containers/text_container_with_background_image/text_container_with_background_image';
import { SquareImagesStaticGallery } from '../../galleries/square_images_static_gallery/square_images_static_gallery';
import { formatDate } from '../../helpers/dates';
import { getArticleById } from '../../store/repositories/articles_repository';

function ImageWithCaptionText(props) {
    return (
        <div>
            <img
                src={props.img.src}
                alt={props.img.alt}
                className="main-image"
            />
            <p className="caption-text">{ props.img.captionText}</p>
        </div>
    );
}

function ImageWithCaptionTextWrapper(props) {
    return props.img ? <ImageWithCaptionText img={props.img} /> : <></>;
}

function SquareImagesStaticGalleryWrapper(props) {
    if (props.images) {
        return <SquareImagesStaticGallery images={props.images} />;
    }
    return <></>;
}

function ArticleContainer(props) {
    const headingWithIntroContainer = (
        <div>
            <h1>{props.article.heading}</h1>
            <p>{formatDate(props.article.date)}</p>
            <p>{props.article.introduction}</p>
        </div>
    );
    return (
        <main className="article-page">
            <TextContainerWithBackgroundImage
                img={props.article.imgMain}
                textChild={headingWithIntroContainer}
            />
            <div className="content-wrapper">
                <p className="two-columns">{props.article.mainBody}</p>
                <ImageWithCaptionTextWrapper img={props.article.images[0]} />
                <p className="two-columns">{props.article.conclusion}</p>
                <SquareImagesStaticGalleryWrapper images={props.article.images.slice(1)} />
            </div>
        </main>
    );
}

function ArticleContainerWrapper(props) {
    if (props.article) {
        return <ArticleContainer article={props.article} />;
    }
    return <></>;
}

export function ArticlePage() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [article, setArticle] = useState({});
    const { id } = useParams();

    useEffect(() => {
        getArticleById(id)
            .then(
                (articleFetched) => {
                    setArticle(articleFetched);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [id]);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <ArticleContainerWrapper article={article} />;
}
