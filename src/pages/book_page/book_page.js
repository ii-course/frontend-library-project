import React, { useState, useEffect } from 'react';
import '../../scss/wrapper.css';
import './book_portfolio/book_portfolio.css';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { getBookById } from '../../store/repositories/books_repository';
import { BookPortfolio } from './book_portfolio/book_portfolio';
import { addToRecentlySeen } from '../../store/store/recently_seen_books/recently_seen_books_slice';

export function BookPage() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [book, setBook] = useState([]);
    const { id } = useParams();
    const dispatch = useDispatch();

    useEffect(() => {
        getBookById(id)
            .then(
                (bookFetched) => {
                    setBook(bookFetched);
                    dispatch(addToRecentlySeen(id));
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [id]);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <BookPortfolio book={book} />;
}
