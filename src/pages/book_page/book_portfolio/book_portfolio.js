import React from 'react';
import { BookFullDetailsContainer } from '../../../cards/book_cards/book_text_containers/book_full_details_container/book_full_details_container';
import CompactCommentCardsContainerWrapper from '../../../containers/compact_comment_cards_container/compact_comment_cards_container';
import { FullScreenBookInfoContainer } from '../../../cards/book_cards/book_text_containers/full_screen_book_info_container/full_screen_book_info_container';
import { QuoteContainer } from './quote_container/quote_container';
import { EditorialReviews } from './editorial_reviews/editorial_reviews';
import { TextContainerWithBackgroundImage } from '../../../containers/text_container_with_background_image/text_container_with_background_image';
import './book_portfolio.css';
import { RelatedBooksGallery } from '../../../galleries/books_galleries/books_scroll_gallery/related_books_gallery/related_books_gallery';
import RecentlySeenBooksGallery from '../../../galleries/books_galleries/books_scroll_gallery/recently_seen_books_gallery/recently_seen_books_gallery';
import BookCommentsGalleryWithPagination from '../../../galleries/comments_galleries/comment_gallery_with_pagination/book_comments_gallery_with_pagination';
import CommentFormWithFormik from "../../../forms/forms/comment_form/comment_form_with_formik";

function getSection(heading, child) {
    return (
        <section className="content-wrapper">
            <h2>{heading}</h2>
            <>{child}</>
        </section>
    );
}

function getSections(sections) {
    const sectionsElements = [];
    Object.entries(sections).forEach(([heading, child]) => {
        const sectionElement = getSection(heading, child);
        sectionsElements.push(sectionElement);
    });
    return sectionsElements;
}

export function BookPortfolio(props) {
    const sections = {
        Details: <BookFullDetailsContainer book={props.book} />,
        'Reader reviews': <>
            <BookCommentsGalleryWithPagination bookId={props.book.id} />
            <h3>Write your review:</h3>
            <CommentFormWithFormik bookId={props.book.id}/>
        </>,
        'Recently seen': <RecentlySeenBooksGallery />,
    };
    const commentsContainer = (
        <div className="text-on-image-wrapper content-wrapper">
            <CompactCommentCardsContainerWrapper bookId={props.book.id} />
        </div>
    );
    return (
        <main className="book-page">
            <FullScreenBookInfoContainer book={props.book} />
            <p className="content-wrapper description">
                {props.book.details.description}
            </p>
            <QuoteContainer
                quote={props.book.details.quote}
                img={props.book.details.illustrations[0]}
            />
            {getSection('Related books',
                <RelatedBooksGallery bookId={props.book.id} />)}
            <EditorialReviews book={props.book} />
            <TextContainerWithBackgroundImage
                img={props.book.details.illustrations[1]}
                textChild={commentsContainer}
            />
            {getSections(sections)}
        </main>
    );
}
