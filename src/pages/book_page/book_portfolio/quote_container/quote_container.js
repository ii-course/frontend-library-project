import React from 'react';
import { TextContainerWithBackgroundImage } from '../../../../containers/text_container_with_background_image/text_container_with_background_image';

export function QuoteContainer(props) {
    const quoteTextContainer = (
        <div className="quote-wrapper text-on-image-wrapper">
            <p className="quote-mark">&apos;&apos;</p>
            <p className="quote">
                {props.quote}
            </p>
        </div>
    );

    return (
        <TextContainerWithBackgroundImage
            img={props.img}
            textChild={quoteTextContainer}
        />
    );
}
