import React, { useState, useEffect } from 'react';
import { FullScreenAuthorShortenedCard } from '../../../../cards/authors_cards/full_screen_author_shortened_card/full_screen_author_shortened_card';
import { getAuthorById } from '../../../../store/repositories/authors_repository';

function AuthorCard(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [author, setAuthor] = useState({});

    useEffect(() => {
        getAuthorById(props.id)
            .then(
                (authorFetched) => {
                    setAuthor(authorFetched);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.id]);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <FullScreenAuthorShortenedCard img={author.img} biography={author.biography} />;
}

function getAuthorsCards(authorsId) {
    const cards = [];
    authorsId.forEach((authorId) => {
        const card = <AuthorCard id={authorId} />;
        cards.push(card);
    });
    return cards;
}

export function EditorialReviews(props) {
    return (
        <section className="editorial-reviews">
            <h2>Editorial reviews</h2>
            <div className="content-wrapper">
                <h3>About the Book</h3>
                <p className="two-columns">
                    {props.book.details.descriptionFull}
                </p>
                <h3>About the Author</h3>
                <div>
                    {getAuthorsCards(props.book.authorsId)}
                </div>
            </div>
        </section>
    );
}
