import React from 'react';
import ReactDOM from 'react-dom';
import {
    Switch, HashRouter, Route, BrowserRouter,
} from 'react-router-dom';
import { getRoutes } from './paths';
import 'leaflet/dist/leaflet.css';
import './scss/index.css';
import { QueryParamProvider } from 'use-query-params';
import { Provider } from 'react-redux';
import { Footer } from './header_footer/footer/footer';
import { Header } from './header_footer/header/header';
import { store } from './store/store/store';
import ScrollToTop from './pages/helpers/scroll/scroll_to_top';

function withStore(WrappedComponent) {
    return (
        <Provider store={store}>
            <QueryParamProvider ReactRouterRoute={Route}>
                <WrappedComponent />
            </QueryParamProvider>
        </Provider>
    );
}

// eslint-disable-next-line no-unused-vars
function withHashRouter(WrappedComponent) {
    return (<HashRouter>
        <ScrollToTop />
        <WrappedComponent />
    </HashRouter>);
}

function App() {
    return (
        <>
            <Header />
            <Switch>
                {getRoutes()}
            </Switch>
            <Footer />
        </>
    );
}

ReactDOM.render(
    <BrowserRouter>
        <ScrollToTop />
        {withStore(App)}
    </BrowserRouter>,
    document.getElementById('root'),
);
