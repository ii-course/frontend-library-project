import React from 'react';
import { HeaderMenu } from '../menu/menu_header/menu_header';
import { Logo } from '../logo/logo';
import './header.css';
import '../../scss/wrapper.css';
import '../header_footer.css';
import HeaderButtonsContainer from './header_buttons_container/header_buttons_container';

export function Header() {
    return (
        <header className="background-container background-header-container" id="header">
            <div className="header-content-container content-wrapper">
                <Logo />
                <HeaderMenu />
                <HeaderButtonsContainer />
            </div>
        </header>
    );
}
