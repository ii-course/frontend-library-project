import React from 'react';
import { SignUpButton } from '../../../buttons/account_buttons/sign_up_button/sign_up_button';
import { LogInPopup } from '../../../popups/log_in_popup/log_in_popup';
import { Button } from '../../../buttons/button/button';
import { AccountButton } from '../../../buttons/account_buttons/account_button/account_button';
import { LogOutButton } from '../../../buttons/account_buttons/log_out_button/log_out_button';
import {mapStateToProps} from "../../../store/store/map_state_to_props";
import {connect} from "react-redux";

function AccountButtons() {
    return (
        <>
            <AccountButton />
            <LogOutButton />
        </>
    );
}

function AuthButtons() {
    const logInButton = <Button style="unfilled" text="Log In" />;
    return (
        <>
            <SignUpButton />
            <LogInPopup trigger={logInButton} />
        </>
    );
}

function HeaderButtonsContainer(props) {
    return (
        <div className="header-buttons-container">
            {props.user ? <AccountButtons /> : <AuthButtons />}
        </div>
    );
}

export default connect(mapStateToProps)(HeaderButtonsContainer);
