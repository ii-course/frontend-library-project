import React from 'react';
import './logo.css';
import { Link } from 'react-router-dom';

export function Logo() {
    const SRC = './icons/logo.png';
    const ALT = 'Logo: old library building';
    return (
        <div className="logo">
            <Link to="/">
                <img src={SRC} alt={ALT} />
                <p>PUBLIC LIBRARY</p>
                <p>of LOREM IPSUM</p>
            </Link>
        </div>
    );
}
