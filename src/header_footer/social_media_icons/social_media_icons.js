import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faFacebook, faTwitter, faTelegramPlane, faYoutube,
} from '@fortawesome/free-brands-svg-icons';

import './social_media_icons.css';

export function SocialMediaIcons() {
    return (
        <div className="social-media-icons">
            <p>FOLLOW US:</p>
            <div className="social-media-icons-container">
                <a href="https://www.facebook.com/">
                    <FontAwesomeIcon icon={faFacebook} />
                </a>
                <a href="https://web.telegram.org">
                    <FontAwesomeIcon icon={faTelegramPlane} />
                </a>
                <a href="https://twitter.com">
                    <FontAwesomeIcon icon={faTwitter} />
                </a>
                <a href="https://www.youtube.com/about/">
                    <FontAwesomeIcon icon={faYoutube} />
                </a>
            </div>
        </div>
    );
}
