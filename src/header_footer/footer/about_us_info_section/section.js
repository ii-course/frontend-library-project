import React from 'react';
import { MenuOption } from '../../menu/menu_option/menu_option';
import { SocialMediaIcons } from '../../social_media_icons/social_media_icons';
import './section.css';
import { mainOptions } from '../../menu/menu_main_options';

export function AboutUsInfoSection() {
    return (
        <div className="about-us-info-content-container">
            <div className="contacts-and-menu-container">
                <div className="additional-menu">
                    {mainOptions.aboutUs}
                    <MenuOption path="/about_us#faq" text="FAQ" />
                    <MenuOption path="/terms_of_use" text="TERMS OF USE" />
                    <MenuOption path="/privacy_policy" text="PRIVACY POLICY" />
                </div>
                <div>
                    <div className="contacts-container">
                        <p>CONTACTS:</p>
                        <div>
                            <p>+123-45-67</p>
                            <p>example@example.com</p>
                        </div>
                    </div>
                </div>
            </div>
            <SocialMediaIcons />
        </div>
    );
}
