import React from 'react';
import { FooterMenu } from '../menu/menu_footer/menu_footer';
import { Logo } from '../logo/logo';
import './footer.css';
import '../header_footer.css';
import '../../scss/wrapper.css';
import { AboutUsInfoSection } from './about_us_info_section/section';

export function Footer() {
    return (
        <footer className="background-container">
            <div className="footer-content-container content-wrapper">
                <FooterMenu />
                <Logo />
                <AboutUsInfoSection />
            </div>
        </footer>
    );
}
