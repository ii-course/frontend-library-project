import React from 'react';
import './menu_option.css';
import { NavHashLink } from 'react-router-hash-link';
import classNames from 'classnames';

export function MenuOption(props) {
    return (
        <NavHashLink
            activeClassName="menu-option-selected"
            smooth to={props.path}
            className={classNames('menu-option', props.className)}
        >
            {props.text}
        </NavHashLink>
    );
}
