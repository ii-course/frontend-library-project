import React from 'react';
import { mainOptions } from '../menu_main_options';
import './menu_header.css';

export function HeaderMenu() {
    return (
        <nav className="menu menu-header">
            {Object.values(mainOptions)}
        </nav>
    );
}
