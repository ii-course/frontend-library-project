import React from 'react';
import { options } from './menu_options/menu_options';
import './menu_footer.css';

export function FooterMenu() {
    return (
        <nav className="menu menu-footer">
            {options}
        </nav>
    );
}
