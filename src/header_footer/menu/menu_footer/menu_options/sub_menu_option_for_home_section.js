import React from "react";
import {connect} from "react-redux";
import {mapStateToProps} from "../../../../store/store/map_state_to_props";
import {MenuOption} from "../../menu_option/menu_option";
import {LogInPopup} from "../../../../popups/log_in_popup/log_in_popup";

function SubMenuForAccount() {
    return (
        <>
            <MenuOption path="/account" text="Account" className="additional-menu-option" key="account" />
        </>
    );
}

function SubMenuForAuth() {
    const logInMenuOption = <a className="menu-option additional-menu-option" key="logIn">Log In</a>;
    return (
        <>
            <LogInPopup trigger={logInMenuOption} />
            <MenuOption path="/sign_up" text="Sign Up" className="additional-menu-option" key="signUp" />
        </>
    );
}

function SubMenuForHomeSection(props) {
    return props.user ? <SubMenuForAccount /> : <SubMenuForAuth />;
}

export default connect(mapStateToProps)(SubMenuForHomeSection);
