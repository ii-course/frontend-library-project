import React from 'react';
import { MenuOption } from '../../menu_option/menu_option';
import './menu_options.css';
import { mainOptions } from '../../menu_main_options';
import SubMenuForHomeSection from './sub_menu_option_for_home_section';

function SubMenuForCatalogue() {
    return (
        <>
            <MenuOption
                path="/catalogue_main" text="Collections"
                className="additional-menu-option" key="collections"
            />
            <MenuOption
                path="/catalogue_main#fiction" text="Fiction"
                className="additional-menu-option" key="fiction"
            />
            <MenuOption
                path="/catalogue_main#nonFiction" text="Non-fiction"
                className="additional-menu-option" key="nonFiction"
            />
            <MenuOption
                path="/catalogue_main" text="Search"
                className="additional-menu-option" key="search"
            />
            <MenuOption
                path="/catalogue_browser" text="Browser"
                className="additional-menu-option" key="fiction"
            />
        </>
    );
}

function SubMenuForWhatsOn() {
    return (
        <>
            <MenuOption
                path="/whats_on#news" text="NEWS & ARTICLES"
                className="additional-menu-option" key="newsAndArticles"
            />
            <MenuOption
                path="/whats_on#events" text="EVENTS"
                className="additional-menu-option" key="events"
            />
        </>
    );
}

export const options = [
    <div className="menu-section" key="homeSection">
        {mainOptions.home}
        <SubMenuForHomeSection />
    </div>,
    <div className="menu-section" key="catalogueSection">
        {mainOptions.catalogue}
        <SubMenuForCatalogue />
    </div>,
    <div className="menu-section" key="whatsOnSection">
        {mainOptions.whatsOn}
        <SubMenuForWhatsOn />
    </div>,
];
