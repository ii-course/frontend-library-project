import React from 'react';
import { MenuOption } from './menu_option/menu_option';

export const mainOptions = {
    home: <MenuOption path="/" text="HOME" key="home" />,
    catalogue: <MenuOption path="/catalogue_main" text="CATALOGUE" key="catalogue" />,
    whatsOn: <MenuOption path="/whats_on" text="WHAT'S ON" key="what's on" />,
    aboutUs: <MenuOption path="/about_us" text="ABOUT US" key="about us" />,
};
