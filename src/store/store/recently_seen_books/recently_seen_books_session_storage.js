export function setRecentlySeenBooksIdToSessionStorage(booksId) {
    if (booksId) {
        window.sessionStorage.setItem('recentlySeenBooksId', JSON.stringify(booksId));
    }
}

export function getRecentlySeenBooksIdFromSessionStorage() {
    return JSON.parse(window.sessionStorage.getItem('recentlySeenBooksId'));
}

export function removeRecentlySeenBooksIdFromSessionStorage() {
    window.sessionStorage.removeItem('recentlySeenBooksId');
}
