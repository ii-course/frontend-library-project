import { createSlice } from '@reduxjs/toolkit';
import {
    getRecentlySeenBooksIdFromSessionStorage,
    setRecentlySeenBooksIdToSessionStorage,
} from './recently_seen_books_session_storage';

const fromSessionStorage = getRecentlySeenBooksIdFromSessionStorage();

const recentlySeenBooksSlice = createSlice({
    name: 'recentlySeenBooksId',
    initialState: { booksId: fromSessionStorage || []},
    reducers: {
        addToRecentlySeen: (state, action) => {
            const idToAdd = action.payload;
            const updated = [...state.booksId];
            if (!updated.includes(idToAdd)) {
                updated.push(idToAdd);
            }
            state.booksId = updated;
            setRecentlySeenBooksIdToSessionStorage(updated);
        },
    },
});

export const { addToRecentlySeen } = recentlySeenBooksSlice.actions;

export default recentlySeenBooksSlice.reducer;
