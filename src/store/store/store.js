import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { rootSaga } from './root_saga';
import { setUserToSessionStorage } from './user/user_session_storage';
import { rootReducer } from './reducer';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    rootReducer, composeWithDevTools(
        applyMiddleware(sagaMiddleware),
    ),
);

sagaMiddleware.run(rootSaga);

window.addEventListener('unload', () => {
    if (store.getState().user && store.getState().user.data) {
        setUserToSessionStorage(store.getState().user.data);
    }
});

export function getRecentlySeenBooksId() {
    return store.getState().recentlySeenBooksId.booksId;
}

export { store };
