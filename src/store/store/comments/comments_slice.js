import { createSlice } from '@reduxjs/toolkit';

const commentsSlice = createSlice({
    name: 'comments',
    initialState: { addedComments: []},
    reducers: {
        addComment: () => {},
        pushCommentToStore: (state, action) => {
            state.addedComments.push(action.payload);
        },
        setError: (state, action) => {
            state.errorMessage = action.payload.errorMessage;
        },
    },
});

export const { addComment, setError, pushCommentToStore } = commentsSlice.actions;

export default commentsSlice.reducer;
