import { call, put, takeLatest } from 'redux-saga/effects';
import {setError, pushCommentToStore} from './comments_slice';
import {postComment} from "../../repositories/comments_repository";
import {getCurrentDateFromISOString} from "../../../helpers/dates";
import {getCurrentUserId} from "../../repositories/current_user_repository/general";

function formComment(data) {
    return {
        userId: getCurrentUserId(),
        heading: data.heading,
        text: data.text,
        stars: Number.parseInt(data.stars),
        date: getCurrentDateFromISOString(),
        bookId: data.bookId
    };
}

function postCommentOnServer(comment) {
    return new Promise((resolve, reject) => {
        return postComment(comment)
            .then(() => resolve())
            .catch(e => reject(e));
    });
}

function* add(action) {
    try {
        const comment = formComment(action.payload);
        yield call(postCommentOnServer, comment);
        yield put(pushCommentToStore(comment));
    } catch (e) {
        console.error(e);
        yield put(setError({ errorMessage: e.message }));
    }
}

export function* addComment() {
    yield takeLatest('comments/addComment', add);
}
