import { combineReducers } from 'redux';
import userReducer from './user/user_slice';
import recentlySeenBooksReducer from './recently_seen_books/recently_seen_books_slice';
import commentsReducer from './comments/comments_slice';

export const rootReducer = combineReducers({
    user: userReducer,
    recentlySeenBooksId: recentlySeenBooksReducer,
    comments: commentsReducer
});
