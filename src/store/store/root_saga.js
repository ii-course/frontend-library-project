import { all } from 'redux-saga/effects';
import {logInUser, signUpUser} from './user/user_sagas/auth_saga';
import { addFavouriteBook, removeFavouriteBook } from './user/user_sagas/books_sagas/favourite_books_saga';
import { addBorrowedBook, removeBorrowedBook } from './user/user_sagas/books_sagas/borrowed_books_saga';
import { addSubscribedBook, removeSubscribedBook } from './user/user_sagas/books_sagas/subsribed_books_saga';
import { orderBook } from './user/user_sagas/books_sagas/ordering_books_saga';
import {addComment} from "./comments/comments_saga";

export function* rootSaga() {
    yield all([
        logInUser(),
        signUpUser(),
        addFavouriteBook(),
        removeFavouriteBook(),
        addBorrowedBook(),
        removeBorrowedBook(),
        addSubscribedBook(),
        removeSubscribedBook(),
        orderBook(),
        addComment()
    ]);
}
