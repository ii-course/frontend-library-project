import { call, put, takeLatest } from 'redux-saga/effects';
import {getJson, post} from '../../../repositories/fetch';
import { setError, setUser, logIn } from '../user_slice';

function formUser(data) {
    return {
        name: {
            first: data.firstName?.trim(),
            last: data.lastName?.trim()
        },
        location: {
            city: data.location?.trim(),
            country: data.country?.trim()
        },
        email: data.email,
        login: {
            password: data.password
        },
        dob: {
            date: data.birthdate
        },
        registered: {
            date: new Date().toISOString()
        },
        picture: {
            large: "./icons/user.png"
        },
        booksBorrowed: [],
        booksOrdered: [],
        booksReadId: [],
        booksSubscribedId: [],
        booksFavouriteId: []
    };
}

/*
! IMPORTANT NOTE:
As it is only a study front-end project without backend, authorization was left as it is.

At the moment, the purpose of authorization (and this code) is only to show some functionality,
such as a user account and borrowing books. In a real-world website authorization MUST NEVER be handled like this.

If this project is going to be used as a part of a real website, this part of the program SHOULD BE REPLACED
with the correct code.
 */

function getUserByLoginInfo(login, password) {
    return new Promise((resolve, reject) => {
        getJson(`/users?email=${login}&login.password=${password}`) // Authorization MUST NOT be done this way
            .then((users) => users[0])                                  // in a real-world project
            .then((user) => {                                           // (see comment above)
                if (!user) {
                    reject('Invalid login or password');
                }
                resolve(user);
            });
    });
}

function postUserOnServer(userData) {
    return new Promise((resolve, reject) => {
        getJson(`/users?email=${userData.email}`)  // Authorization MUST NOT be done this way
            .then(users => users[0])              // in a real-world project
            .then(user => {                       // (see comment above)
                if (user) {
                    reject('User with this email is already signed up');
                } else {
                    const userToPost = formUser(userData);
                    return post('/users', userToPost)
                        .then(() => resolve(userToPost));
                }
            });
    });
}

function* getUser(action) {
    try {
        const user = yield call(getUserByLoginInfo, action.payload.login, action.payload.password);
        yield put(setUser(user));
    } catch (e) {
        alert(e);
        yield put(setError({ errorMessage: e.message }));
    }
}

function* postUser(action) {
    try {
        const user = yield call(postUserOnServer, action.payload);
        yield put(logIn({login: user.email, password: user.login.password}));
    } catch (e) {
        alert(e);
        yield put(setError({ errorMessage: e.message }));
    }
}

export function* logInUser() {
    yield takeLatest('user/logIn', getUser);
}

export function* signUpUser() {
    yield takeLatest('user/signUp', postUser);
}
