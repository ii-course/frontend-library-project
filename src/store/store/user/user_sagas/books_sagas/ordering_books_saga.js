import { call, put, takeLatest } from 'redux-saga/effects';
import { setError, setOrderedBooks } from '../../user_slice';
import { addOrderedBookWithFetch } from '../../../../repositories/current_user_repository/books/ordered_books';

function* order(action) {
    try {
        const updatedBooks = yield call(addOrderedBookWithFetch, action.payload);
        yield put(setOrderedBooks(updatedBooks));
    } catch (e) {
        console.error(e);
        yield put(setError({ errorMessage: e.message }));
    }
}

export function* orderBook() {
    yield takeLatest('user/orderBook', order);
}
