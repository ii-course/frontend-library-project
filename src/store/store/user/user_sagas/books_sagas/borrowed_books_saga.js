import { call, put, takeLatest } from 'redux-saga/effects';
import { setError, setBorrowedBooks, setReadBooksId } from '../../user_slice';
import {
    addBorrowedBookWithFetch,
    removeFromBorrowedBooksWithFetch,
} from '../../../../repositories/current_user_repository/books/borrowed_books';
import {addBookIdToReadWithFetch} from "../../../../repositories/current_user_repository/books/read_books";

function* remove(action) {
    try {
        const updatedBorrowedBooks = yield call(removeFromBorrowedBooksWithFetch, action.payload);
        const updatedReadBooks = yield call(addBookIdToReadWithFetch, action.payload);
        yield put(setBorrowedBooks(updatedBorrowedBooks));
        yield put(setReadBooksId(updatedReadBooks));
    } catch (e) {
        console.error(e);
        yield put(setError({ errorMessage: e.message }));
    }
}

function* add(action) {
    try {
        const updatedBooks = yield call(addBorrowedBookWithFetch, action.payload);
        yield put(setBorrowedBooks(updatedBooks));
    } catch (e) {
        console.error(e);
        yield put(setError({ errorMessage: e.message }));
    }
}

export function* addBorrowedBook() {
    yield takeLatest('user/addBorrowedBook', add);
}

export function* removeBorrowedBook() {
    yield takeLatest('user/removeBorrowedBook', remove);
}
