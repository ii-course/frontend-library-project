import { call, put, takeLatest } from 'redux-saga/effects';
import {
    addFavouriteBookWithFetch, removeFromFavouriteBooksWithFetch,
} from '../../../../repositories/current_user_repository/books/favourite_books';
import { setError, setFavouriteBooksId } from '../../user_slice';

function* remove(action) {
    try {
        const updatedBooksId = yield call(removeFromFavouriteBooksWithFetch, action.payload);
        yield put(setFavouriteBooksId(updatedBooksId));
    } catch (e) {
        console.error(e);
        yield put(setError({ errorMessage: e.message }));
    }
}

function* add(action) {
    try {
        const updatedBooksId = yield call(addFavouriteBookWithFetch, action.payload);
        yield put(setFavouriteBooksId(updatedBooksId));
    } catch (e) {
        console.error(e);
        yield put(setError({ errorMessage: e.message }));
    }
}

export function* addFavouriteBook() {
    yield takeLatest('user/addFavouriteBook', add);
}

export function* removeFavouriteBook() {
    yield takeLatest('user/removeFavouriteBook', remove);
}
