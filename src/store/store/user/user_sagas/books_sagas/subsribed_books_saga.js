import { call, put, takeLatest } from 'redux-saga/effects';
import { setError, setSubscribedBooksId } from '../../user_slice';
import {
    addSubscribedBookWithFetch,
    removeFromSubscribedBooksWithFetch,
} from '../../../../repositories/current_user_repository/books/subscribed_books';

function* remove(action) {
    try {
        const updatedBooksId = yield call(removeFromSubscribedBooksWithFetch, action.payload);
        yield put(setSubscribedBooksId(updatedBooksId));
    } catch (e) {
        yield put(setError({ errorMessage: e.message }));
    }
}

function* add(action) {
    try {
        const updatedBooksId = yield call(addSubscribedBookWithFetch, action.payload);
        yield put(setSubscribedBooksId(updatedBooksId));
    } catch (e) {
        yield put(setError({ errorMessage: e.message }));
    }
}

export function* addSubscribedBook() {
    yield takeLatest('user/addSubscribedBook', add);
}

export function* removeSubscribedBook() {
    yield takeLatest('user/removeSubscribedBook', remove);
}
