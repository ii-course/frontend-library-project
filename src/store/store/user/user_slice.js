import { createSlice } from '@reduxjs/toolkit';
import { getUserFromSessionStorage, removeUserFromSessionStorage, setUserToSessionStorage } from './user_session_storage';

export const userSlice = createSlice({
    name: 'user',
    initialState: { data: getUserFromSessionStorage() },
    reducers: {
        logIn: () => {},
        logOut: (state) => {
            // eslint-disable-next-line no-unused-vars
            state.data = null;
            removeUserFromSessionStorage();
        },
        signUp: () => {},
        setUser: (state, action) => {
            console.log( action.payload);
            state.data = action.payload;
            setUserToSessionStorage(action.payload);
        },
        addFavouriteBook: () => {},
        removeFavouriteBook: () => {},
        setFavouriteBooksId: (state, action) => {
            state.data.booksFavouriteId = action.payload.booksFavouriteId;
        },
        addBorrowedBook: () => {},
        removeBorrowedBook: () => {},
        setBorrowedBooks: (state, action) => {
            state.data.booksBorrowed = action.payload.booksBorrowed;
        },
        addSubscribedBook: () => {},
        removeSubscribedBook: () => {},
        setSubscribedBooksId: (state, action) => {
            state.data.booksSubscribedId = action.payload.booksSubscribedId;
        },
        orderBook: () => {},
        setOrderedBooks: (state, action) => {
            state.data.booksOrdered = action.payload.booksOrdered;
        },
        setReadBooksId: (state, action) => {
            state.data.booksReadId = action.payload.booksReadId;
        },
        setError: (state, action) => {
            state.errorMessage = action.payload.errorMessage;
        },
    },
});

export const {
    logIn, logOut, setUser, setError,
    addSubscribedBook, addBorrowedBook, addFavouriteBook, removeBorrowedBook,
    removeFavouriteBook, removeSubscribedBook, setBorrowedBooks,
    setFavouriteBooksId, setSubscribedBooksId, orderBook, setOrderedBooks,signUp, setReadBooksId
} = userSlice.actions;

export default userSlice.reducer;
