export const mapStateToProps = (state) => {
    return {
        user: state.user.data,
        recentlySeenBooksId: state.recentlySeenBooksId.booksId,
        addedComments: state.comments.addedComments
    };
};
