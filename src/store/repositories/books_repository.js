import qs from 'query-string';
import { getJson, getCount } from './fetch';

const MAP = {
    year: 'details.year',
    section: 'details.section',
    genres_like: 'details.genres_like',
    heading_like: 'details.heading_like',
    genres: 'details.genres',
    country: 'details.country',
    language: 'details.language',
    format: 'details.format',
    availability: 'details.available',
    _sort: '_sort',
    _order: '_order',
    sort: '_sort',
    order: '_order',
    asc: 'asc',
    desc: 'desc',
    name: 'details.heading',
};

function map(value) {
    if (MAP[value]) {
        return MAP[value];
    }
    return value;
}

function formatParams(params) {
    const formattedParams = {};
    Object.entries(params).forEach(([key, value]) => {
        formattedParams[map(key)] = map(value);
    });
    return formattedParams;
}

export function isEbook(bookId) {
    return getBookById(bookId)
        .then((book) => book.detail.format === 'E-book');
}

export function getBooksByGenre(genre) {
    return getJson(`/books?details.genres_like=${genre}`);
}

export function getBooksByGenreLimit(genre, limit = 10) {
    return getJson(`/books?details.genres_like=${genre}&_start=0&_limit=${limit}`);
}

export function getBookById(id) {
    return getJson(`/books?id=${id}`)
        .then((books) => books[0]);
}

export function getRelatedBooks(id) {
    return getBookById(id)
        .then((book) => {
            const promises = [];
            book.relatedBooksId.forEach((relatedBookId) => {
                promises.push(getBookById(relatedBookId));
            });
            return Promise.all(promises);
        });
}

export function getAllBooksByIds(booksId) {
    const promises = [];
    booksId.forEach((id) => {
        promises.push(getBookById(id));
    });
    return Promise.all(promises);
}

export function getBooksByAuthor(authorId, page = 1, limit = 10) {
    return getJson(`/books?authorsId_like=${authorId}&_page=${page}$_limit=${limit}`);
}

export function getBooksByAuthorCount(authorId) {
    return getCount(`/books?authorsId_like=${authorId}`);
}

export function getAllBooksDetailValues(detail) {
    return getJson('/books')
        .then((books) => {
            const details = books.map((book) => book.details[detail]);
            const distinctDetails = [...new Set(details)];
            distinctDetails.sort();
            return distinctDetails;
        });
}

export function getAllBooksYears() {
    return getAllBooksDetailValues('year');
}

export function getAllBooksCount() {
    return getCount('/books?_page=1');
}

export function getAllBooks() {
    return getJson('/books');
}

export function getBooksByParamsCount(params) {
    if (!params) {
        return getAllBooksCount();
    }
    const formattedParams = formatParams(params);
    return getCount(`/books?${qs.stringify(formattedParams)}`);
}

function arePropertiesEmpty(obj) {
    const value = Object.values(obj).find(value => !!value);
    return !value || (Array.isArray(value) && value.length === 0);
}

function areParamsEmpty(params) {
    // eslint-disable-next-line no-unused-vars
    const {_page, _limit, ...otherParams} = params;
    return !otherParams || arePropertiesEmpty(otherParams);
}

export function getAllBooksByParams(params, emptyOption = false) {
    if (areParamsEmpty(params) && emptyOption) {
        return new Promise((resolve) => resolve([]));
    }
    if (!params) {
        return getAllBooks();
    }
    const formattedParams = formatParams(params);
    return getJson(`/books?${qs.stringify(formattedParams)}`, false);
}

export function getAllBooksByParamsEmptyIfNoParams(params) {
    if (!params) {
        return [];
    }
    const formattedParams = formatParams(params);
    return getJson(`/books?${qs.stringify(formattedParams)}`, false);
}
