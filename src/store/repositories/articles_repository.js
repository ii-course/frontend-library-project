import { getJson } from './fetch';

export function getAllArticles() {
    const url = '/articles?_sort=date&_order=desc';
    return getJson(url);
}

export function getArticleById(id) {
    const url = `/articles?id=${id}`;
    return getJson(url)
        .then((response) => response[0]);
}
