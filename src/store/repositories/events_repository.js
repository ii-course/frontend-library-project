import { getJson, getCount } from './fetch';

export function getEvents(eventType, page = 1, limit = 10) {
    const url = `/${eventType}?_sort=date&_order=desc&_page=${page}&_limit=${limit}`;
    return getJson(url);
}

export function getAllEventsCount(eventType) {
    const url = `/${eventType}?_page=1`;
    return getCount(url);
}

export function getEventById(eventType, id) {
    const url = `/${eventType}?id=${id}`;
    return getJson(url)
        .then((response) => response[0]);
}
