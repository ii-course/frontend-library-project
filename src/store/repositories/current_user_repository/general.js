import {patch} from '../fetch';
import {store} from "../../store/store";

export function getCurrentUserId() {
    return store.getState().user.data.id;
}

export function updateCurrentUser(body) {
    const userId = getCurrentUserId();
    return patch(`/users/${userId}`, body)
        .then(() => body);
}

export function getCurrentUser() {
    return store.getState().user.data;
}

export function isUserLoggedIn() {
    return !!store.getState().user && !!store.getState().user.data;
}
