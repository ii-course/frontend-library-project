import {getCurrentUser, isUserLoggedIn, updateCurrentUser} from '../general';

function updateFavouriteBooksId(booksId) {
    return updateCurrentUser({ booksFavouriteId: booksId });
}

export function removeFromFavouriteBooksWithFetch(bookId) {
    const booksId = [...getCurrentUser().booksFavouriteId].filter((id) => id !== bookId);
    return updateFavouriteBooksId(booksId);
}

export function addFavouriteBookWithFetch(bookId) {
    const booksId = [...getCurrentUser().booksFavouriteId];
    booksId.push(bookId);
    return updateFavouriteBooksId(booksId);
}

export function isFavourite(bookId) {
    if (isUserLoggedIn()) {
        return getCurrentUser().booksFavouriteId.includes(bookId);
    }
    return false;
}
