import {getCurrentUser, isUserLoggedIn, updateCurrentUser} from '../general';

function updateOrderedBooks(books) {
    return updateCurrentUser({ booksOrdered: books });
}

export function addOrderedBookWithFetch(book) {
    const booksId = [...getCurrentUser().booksOrdered];
    booksId.push(book);
    return updateOrderedBooks(booksId);
}

export function isOrdered(bookId) {
    if (isUserLoggedIn()) {
        return !!getCurrentUser().booksOrdered.find((book) => book.bookId === bookId);
    }
    return false;
}
