import {getCurrentUser, isUserLoggedIn, updateCurrentUser} from '../general';

function updateBorrowedBooks(books) {
    return updateCurrentUser({ booksBorrowed: books });
}

export function removeFromBorrowedBooksWithFetch(bookId) {
    const books = [...getCurrentUser().booksBorrowed].filter((book) => book.bookId !== bookId);
    return updateBorrowedBooks(books);
}

function formBorrowedBookRecord(bookId) {
    const DEFAULT_PERIOD_IN_DAYS = 21;
    const endDate = new Date();
    endDate.setDate(endDate.getDate() + DEFAULT_PERIOD_IN_DAYS);
    return {
        bookId,
        startDate: new Date().toISOString(),
        endDate: endDate.toISOString(),
    };
}

export function addBorrowedBookWithFetch(bookId) {
    const books = [...getCurrentUser().booksBorrowed];
    const record = formBorrowedBookRecord(bookId);
    books.push(record);
    return updateBorrowedBooks(books);
}

export function isBorrowed(bookId) {
    if (isUserLoggedIn()) {
        return !!getCurrentUser().booksBorrowed.find((book) => book.bookId === bookId);
    }
    return false;
}
