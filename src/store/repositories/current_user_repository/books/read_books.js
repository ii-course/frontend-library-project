import {getCurrentUser, updateCurrentUser} from "../general";

function updateReadBooks(booksId) {
    return updateCurrentUser({ booksReadId: booksId });
}

export function addBookIdToReadWithFetch(bookId) {
    const booksId = [...getCurrentUser().booksReadId];
    if (!booksId.includes(bookId)) {
        booksId.push(bookId);
    }
    return updateReadBooks(booksId);
}
