import {getCurrentUser, isUserLoggedIn, updateCurrentUser} from '../general';

function updateSubscribedBooksId(booksId) {
    return updateCurrentUser({ booksSubscribedId: booksId });
}

export function removeFromSubscribedBooksWithFetch(bookId) {
    const booksId = [...getCurrentUser().booksSubscribedId].filter((id) => id !== bookId);
    return updateSubscribedBooksId(booksId);
}

export function addSubscribedBookWithFetch(bookId) {
    const booksId = [...getCurrentUser().booksSubscribedId];
    booksId.push(bookId);
    return updateSubscribedBooksId(booksId);
}

export function isInSubscribed(bookId) {
    if (isUserLoggedIn()) {
        return getCurrentUser().booksSubscribedId.includes(bookId);
    }
    return false;
}
