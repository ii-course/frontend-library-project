import { getJson, getCount, post} from './fetch';

export function getCommentsByBookId(bookId, page = 1) {
    return getJson(`/comments?_sort=date&_order=desc&bookId=${bookId}&_page=${page}`);
}

export function getCommentsByUserId(userId, page = 1) {
    return getJson(`/comments?_sort=date&_order=desc&userId=${userId}&_page=${page}`);
}

export function getCommentsByBookIdCount(bookId) {
    return getCount(`/comments?bookId=${bookId}&_page=1`);
}

export function getCommentsByUserIdCount(userId) {
    return getCount(`/comments?userId=${userId}}&_page=1`);
}

export function postComment(comment) {
    return post('/comments', comment);
}
