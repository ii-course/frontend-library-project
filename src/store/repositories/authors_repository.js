import qs from 'query-string';
import {getJson, getCount} from './fetch';
import { getFullNameReversed } from '../../helpers/strings';
import { getAllBooksByParams } from './books_repository';

export function getAuthorById(id) {
    return getJson(`/authors?id=${id}`)
        .then((authors) => authors[0]);
}

export function getAuthorsByIds(authorsId) {
    const promises = [];
    authorsId.forEach((id) => {
        promises.push(getAuthorById(id));
    });
    return Promise.all(promises);
}

function formatParams(params) {
    // eslint-disable-next-line no-unused-vars
    const {_sort, _order, ...newParams} = params;
    return newParams;
}

export function getAllAuthorsByBooksParams(booksParams) {
    const params = formatParams(booksParams);
    return getAllBooksByParams(params)
        .then((books) => {
            const authorsId = books.map((book) => book.authorsId);
            const flatted = authorsId.flat(Infinity);
            const uniqueAuthorsId = [...new Set(flatted)];
            return getAuthorsByIds(uniqueAuthorsId);
        });
}

export function getAllAuthorsNames() {
    return getAllAuthors()
        .then((authors) => {
            const names = authors.map((author) => getFullNameReversed(author.name));
            names.sort();
            return names;
        });
}

export function getAllAuthorsByParams(params) {
    return getJson(`/authors?${qs.stringify(params)}`);
}

export function getAllAuthorsCount() {
    return getCount('/authors?_page=1');
}

export function getAllAuthors() {
    return getJson('/authors');
}
