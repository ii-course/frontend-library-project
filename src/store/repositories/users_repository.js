import {getCount, getJson} from './fetch';

export function getAllUsersCount() {
    return getCount('/users?_page=1');
}

export function getUserById(id) {
    return getJson(`/users?id=${id}`)
        .then((users) => users[0]);
}
