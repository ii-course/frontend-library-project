import React from 'react';
import { StarIconsContainer } from '../../../buttons/icon_buttons/icon_buttons_containers/star_icons_container/star_icons_container';
import './compact_comment_card.css';
import { getFirstSentence } from '../../../helpers/strings';

export function CompactCommentCard(props) {
    const firstSentence = getFirstSentence(props.text);
    return (
        <div className="compact-comment-card">
            <StarIconsContainer starsFilled={props.stars} />
            <p>{props.heading}</p>
            <p className="caption-text">{firstSentence}</p>
        </div>
    );
}
