import React, { useState, useEffect } from 'react';
import './full_comment_card.css';
import { CommentContentContainer } from '../comment_content_container/comment_content_container';
import { getFullName } from '../../../helpers/strings';
import { getUserById } from '../../../store/repositories/users_repository';

function FullCommentCard(props) {
    const userFullName = getFullName(props.user.name);
    return (
        <div className="full-comment-card" key={props.comment.id.toString()}>
            <div>
                <div className="full-comment-card-img-wrapper">
                    <img
                        src={props.user.picture.large}
                        alt={userFullName}
                    />
                </div>
            </div>
            <div>
                <p>{userFullName}</p>
                <CommentContentContainer comment={props.comment} />
            </div>
        </div>
    );
}

export function FullCommentCardWrapper(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [user, setUser] = useState([]);

    useEffect(() => {
        getUserById(props.comment.userId)
            .then(
                (userFetched) => {
                    setUser(userFetched);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.comment.userId]);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <FullCommentCard comment={props.comment} user={user} />;
}
