import React from 'react';
import { StarIconsContainer } from '../../../buttons/icon_buttons/icon_buttons_containers/star_icons_container/star_icons_container';
import { formatDate } from '../../../helpers/dates';

export function CommentContentContainer(props) {
    return (
        <>
            <h3>{props.comment.heading}</h3>
            <StarIconsContainer starsFilled={props.comment.stars} />
            <p>{formatDate(props.comment.date)}</p>
            <p>{props.comment.text}</p>
        </>
    );
}
