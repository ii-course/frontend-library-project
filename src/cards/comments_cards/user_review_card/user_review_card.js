import React, { useEffect, useState } from 'react';
import { CommentContentContainer } from '../comment_content_container/comment_content_container';
import './user_review_card.css';
import '../../book_cards/book_cover_static.css';
import { AuthorsButtonsContainer } from '../../book_cards/book_text_containers/book_short_details_container/authors_buttons_container/authors_buttons_container';
import { getBookById } from '../../../store/repositories/books_repository';

export function UserReviewCard(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [book, setBook] = useState({});

    useEffect(() => {
        getBookById(props.comment.bookId)
            .then(
                (result) => {
                    setBook(result);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.comment.bookId]);

    if (error) {
        return (
            <div>
              Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return (
        <div className="user-review-card">
            <img
                src={book.details.cover.src}
                alt={book.details.cover.alt}
                className="book-cover-static"
            />
            <div>
                <p className="book-heading">{book.details.heading}</p>
                <AuthorsButtonsContainer
                    authorsId={book.authorsId}
                    year={book.details.year}
                />
                <CommentContentContainer comment={props.comment} />
            </div>
        </div>
    );
}
