import React from 'react';
import { Button } from '../../../buttons/button/button';
import './container.css';
import { formatDate } from '../../../helpers/dates';
import { getFirstNSentences } from '../../../helpers/strings';

function getHeadingElement(text, type) {
    if (type === 'h3') {
        return <h3 className="heading">{text}</h3>;
    }
    return <h2 className="heading">{text}</h2>; // default
}

export function ArticleTextContainer(props) {
    const headingElement = getHeadingElement(props.article.heading, props.article.headingType);
    const text = getFirstNSentences(props.article.introduction, 3);
    return (
        <div className="article-text-container">
            <div>
                {headingElement}
                <p>{formatDate(props.article.date)}</p>
                <p>{text}</p>
            </div>
            <Button text="read more" style={props.button.style} path={props.button.path} />
        </div>
    );
}
