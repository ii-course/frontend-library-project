import React from 'react';
import { CardWithImage } from '../card_with_image';
import './card_with_top_image.css';

export function CardWithTopImage(props) {
    return (
        <CardWithImage
            className="card-with-top-image"
            article={props.article}
            button={props.button}
        />
    );
}
