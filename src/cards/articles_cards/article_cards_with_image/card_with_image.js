import React from 'react';
import { ArticleTextContainer } from '../article_text_container/container';

export function CardWithImage(props) {
    const textContainer = (
        <ArticleTextContainer
            article={props.article}
            button={props.button}
        />
    );
    return (
        <div className={props.className}>
            <div className={props.gradient}>
                <img src={props.article.imgMain.src} alt={props.article.imgMain.alt} />
            </div>
            <div className="text-container">
                {textContainer}
            </div>
        </div>
    );
}
