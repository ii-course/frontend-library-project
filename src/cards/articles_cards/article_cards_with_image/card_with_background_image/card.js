import React from 'react';
import { CardWithImage } from '../card_with_image';
import './card.css';

export function CardWithBackgroundImage(props) {
    return (
        <CardWithImage
            className="card-with-background-image"
            gradient="img-gradient"
            article={props.article}
            button={props.button}
        />
    );
}
