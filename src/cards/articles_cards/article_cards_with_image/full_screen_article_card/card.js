import React from 'react';
import { Button } from '../../../../buttons/button/button';
import './card.css';

export function FullScreenArticleCard(props) {
    return (
        <div className="full-screen-article-card">
            <div className="full-screen-article-card-img-container">
                <img src={props.img.src} alt={props.img.alt} />
            </div>
            <div className="full-screen-article-card-text-container">
                <h1>{props.heading}</h1>
                <p>{props.text}</p>
                <Button
                    style="filled"
                    text="Read more"
                    path={props.path}
                />
            </div>
        </div>
    );
}
