import React from 'react';
import { SmallEventTextContainer } from '../../event_text_container/small_event_text_container/small_event_text_container';
import './event_card_with_square_image.css';
import { ArrowButtonRight } from '../../../../buttons/icon_buttons/arrow_button/arrow_button';
export function EventCardWithSquareImage(props) {
    return (
        <div className="square-img-event-card">
            <div className="event-card-wrapper">
                <ArrowButtonRight color="white" onClick={props.onClick} />
            </div>
            <div className="event-card-img-wrapper">
                <div className="square-img-event-card-img-wrapper">
                    <img src={props.img.src} alt={props.img.alt} className="square-img-event-card-img" />
                </div>
            </div>
            <SmallEventTextContainer
                heading={props.heading}
                date={props.date}
                button={props.button}
                id={props.id}
            />
        </div>
    );
}
