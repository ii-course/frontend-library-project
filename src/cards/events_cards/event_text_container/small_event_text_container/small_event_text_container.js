import React from 'react';
import { Button } from '../../../../buttons/button/button';
import '../event_text_container.css';
import {formatDate} from "../../../../helpers/dates";

export function SmallEventTextContainer(props) {
    return (
        <div className="event-text-container">
            <h3>{props.heading}</h3>
            <p className="event-text-container-date">{formatDate(props.date)}</p>
            <Button text="see more" style={props.button.style} path={`/events/exhibitions/${props.id}`} />
        </div>
    );
}
