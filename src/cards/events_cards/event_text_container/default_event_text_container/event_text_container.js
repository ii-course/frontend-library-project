import React from 'react';
import { Button } from '../../../../buttons/button/button';
import '../event_text_container.css';
import {formatDate} from "../../../../helpers/dates";

export function EventTextContainer(props) {
    return (
        <div className="event-text-container" key={props.event.id}>
            <h3>{props.event.heading}</h3>
            <p className="event-text-container-date">{formatDate(props.event.date)}</p>
            <p>{`WHEN: ${props.event.when}`}</p>
            <p>{`WHERE: ${props.event.where}`}</p>
            <p>{props.text}</p>
            <Button text="details" style={props.button.style} path={`/events/${props.eventType}/${props.event.id}`} />
        </div>
    );
}
