import React from 'react';
import { HashLink } from 'react-router-hash-link';
import BookIconButtonsContainer from '../../../buttons/icon_buttons/icon_buttons_containers/book_icon_buttons_container/book_icon_buttons_container';
import { AuthorsNames } from '../authors_names';
import './book_card_compact.css';

export function CompactBookCard(props) {
    let className = 'compact-book-card ';
    if (!props.book.details.available) {
        className += 'book-not-available';
    }
    return (
        <div className={className}>
            <div className="compact-book-card-img-wrapper">
                <BookIconButtonsContainer
                    buttonsState={props.book.buttonsState}
                    book={props.book}
                />
                <img
                    className="compact-book-card-img"
                    src={props.book.details.cover.src}
                    alt={props.book.details.cover.alt}
                />
            </div>
            <div className="caption">
                <HashLink to={`/books/${props.book.id}`} className="compact-book-card-heading">
                    {props.book.details.heading}
                </HashLink>
                <AuthorsNames className="compact-book-card-author" authorsId={props.book.authorsId} />
            </div>
        </div>
    );
}
