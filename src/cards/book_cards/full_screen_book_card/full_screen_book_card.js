import React from 'react';
import { Button } from '../../../buttons/button/button';
import './full_screen_book_card.css';
import '../../../scss/wrapper.css';
import { FullScreenBookInfoContainer } from '../book_text_containers/full_screen_book_info_container/full_screen_book_info_container';

export function FullScreenBookCard(props) {
    const button = <Button style="filled" text="show more" path={`/books/${props.book.id}`} />;
    return (
        <FullScreenBookInfoContainer
            className="filled-full-screen-book-card"
            book={props.book}
            heading="h3"
            button={button}
        />
    );
}
