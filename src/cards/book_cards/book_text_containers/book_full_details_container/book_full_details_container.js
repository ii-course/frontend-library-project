import React from 'react';
import { getInfoFieldsWithLabels, getPublicationDateFormatted } from '../info_fields_formation';
import { AuthorsLinkButtons } from '../../authors_link_buttons';

export function BookFullDetailsContainer(props) {
    const fieldsToDisplay = ['year', 'language', 'ISBN-13', 'age', 'section', 'genres',
        'pages', 'format', 'hasIllustrations', 'translation', 'publisher'];
    return (
        <div>
            <div className="dossier-container" key="authors">
                <span className="dossier-label">Author:</span>
                <AuthorsLinkButtons authorsId={props.book.authorsId} />
            </div>
            {getInfoFieldsWithLabels(fieldsToDisplay, props.book.details)}
            {getPublicationDateFormatted(props.book.details.publicationDate)}
        </div>
    );
}
