import React from 'react';
import './book_short_details_container.css';
import { getInfoFieldsWithLabels, getPublicationDateFormatted } from '../info_fields_formation';
import { AuthorsButtonsContainer } from './authors_buttons_container/authors_buttons_container';

export function BookShortDetailsContainer(props) {
    const fieldsToDisplay = ['format', 'language', 'pages', 'publisher'];
    return (
        <div className="book-short-details-container">
            <AuthorsButtonsContainer
                authorsId={props.book.authorsId}
                button={props.button}
                year={props.book.details.year}
            />
            {getInfoFieldsWithLabels(fieldsToDisplay, props.book.details)}
            {getPublicationDateFormatted(props.book.details.publicationDate)}
        </div>
    );
}
