import React from 'react';
import { AuthorsLinkButtons } from '../../../authors_link_buttons';

export function AuthorsButtonsContainer(props) {
    return (
        <div className="info-container" key="authors">
            <span>{'by '}</span>
            <AuthorsLinkButtons authorsId={props.authorsId} />
            <span>{` (${props.year})`}</span>
        </div>
    );
}
