import React from 'react';
import { Button } from '../../../buttons/button/button';
import { formatDate } from '../../../helpers/dates';
import '../../../containers/dossier_container.css';

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

function formLabelString(string) {
    const abbreviations = ['ISBN', 'ISBN-13', 'ISBN-11'];
    if (abbreviations.includes(string)) {
        return string;
    }
    const fromCamelCase = string.replace(/([A-Z])/g, ' $1');
    return capitalizeFirstLetter(fromCamelCase);
}

export function getAuthorsButtons(authors, authorsButton = { style: 'dark-link' }) {
    const elements = [];
    authors.forEach((author, index) => {
        const buttonContainerElements = [];
        const button = (
            <Button
                style={authorsButton.style}
                text={author.name}
                onClick={author.onClick}
            />
        );
        buttonContainerElements.push(button);
        if (index < authors.length - 1) {
            const comma = <span key={index}>{', '}</span>;
            buttonContainerElements.push(comma);
        }
        const buttonContainer = (
            <div className="button-container" key={author.id}>
                {buttonContainerElements}
            </div>
        );
        elements.push(buttonContainer);
    });
    return elements;
}

function getTextToDisplay(rawData, field) {
    if (!rawData) {
        return '-';
    }
    if (rawData === true) {
        return 'yes';
    }
    if (rawData === false) {
        return 'no';
    }
    if (Array.isArray(rawData)) {
        return rawData.join(', ');
    }
    if (field === 'age') {
        return `${rawData}+`;
    }
    return rawData.toString();
}

export function getInfoFieldsWithLabels(fields, details) {
    const elements = [];
    fields.forEach((field, index) => {
        const textToDisplay = getTextToDisplay(details[field], field);
        const element = (
            <div className="dossier-container" key={index.toString()}>
                <span className="dossier-label">{`${formLabelString(field)}: `}</span>
                <span>{textToDisplay}</span>
            </div>
        );
        elements.push(element);
    });
    return elements;
}

export function getPublicationDateFormatted(date) {
    return (
        <div className="dossier-container" key="publicationDate">
            <span className="dossier-label">{'Publication date: '}</span>
            <span>{formatDate(date)}</span>
        </div>
    );
}
