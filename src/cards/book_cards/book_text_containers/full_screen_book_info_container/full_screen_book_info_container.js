import React from 'react';
import BookIconButtonsContainer from '../../../../buttons/icon_buttons/icon_buttons_containers/book_icon_buttons_container/book_icon_buttons_container';
import { BookShortDetailsContainer } from '../book_short_details_container/book_short_details_container';
import CompactCommentCardsContainerWrapper from '../../../../containers/compact_comment_cards_container/compact_comment_cards_container';
import './full_screen_book_info_container.css';

export function FullScreenBookInfoContainer(props) {
    let heading = <h1>{props.book.details.heading}</h1>;
    if (props.heading === 'h3') {
        heading = <h3>{props.book.details.heading}</h3>;
    }
    const comments = props.displayComments ? <CompactCommentCardsContainerWrapper bookId={props.book.id} /> : <></>;
    const isFilled = props.className === 'filled-full-screen-book-card';
    return (
        <div className={`full-screen-book-card ${props.className ? props.className : ''}`}>
            <div className="content-wrapper">
                <BookIconButtonsContainer
                    colour={isFilled ? 'white' : 'black'}
                    buttonsState={props.book.buttonsState}
                    book={props.book}
                />
                <div className="full-screen-book-card-info-container">
                    <div key="imgContainer" className="img-container">
                        <img
                            className="full-screen-book-card-img"
                            src={props.book.details.cover.src}
                            alt={props.book.details.cover.alt}
                        />
                    </div>
                    <div key="infoContainer" className="heading-with-details-container">
                        {heading}
                        <div className="details-container">
                            <BookShortDetailsContainer
                                button={{ style: isFilled ? 'light-link' : 'dark-link' }}
                                book={props.book}
                            />
                        </div>
                        {props.button ? props.button : <></>}
                    </div>
                </div>
                {comments}
            </div>
        </div>
    );
}
