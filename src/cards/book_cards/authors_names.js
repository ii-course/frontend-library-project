import React, { useState, useEffect } from 'react';
import { getAuthorsByIds } from '../../store/repositories/authors_repository';
import { getFullName } from '../../helpers/strings';

export function getAuthorsNames(authors) {
    const names = [];
    authors.forEach((author) => {
        names.push(
            getFullName(author.name),
        );
    });
    return names.join(', ');
}

export function AuthorsNames(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [authors, setAuthors] = useState([]);

    useEffect(() => {
        getAuthorsByIds(props.authorsId)
            .then(
                (result) => {
                    setAuthors(result);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.authorsId]);

    if (error) {
        return (
            <div>
              Error:
                {error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return (
        <p className={props.className}>
            {getAuthorsNames(authors)}
        </p>
    );
}
