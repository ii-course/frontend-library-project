import React from 'react';
import { BookShortDetailsContainer } from '../book_text_containers/book_short_details_container/book_short_details_container';
import './borrowed_book_card.css';
import { Button } from '../../../buttons/button/button';
import '../book_cover_static.css';
import {useDispatch} from "react-redux";
import {removeBorrowedBook} from "../../../store/store/user/user_slice";

function ReturnButton(props) {
    return props.isButtonAvailable
        ? <Button style="filled" text="return" onClick={props.onClick} /> : <></>;
}

export function BorrowedBookCard(props) {
    const dispatch = useDispatch();
    const removeFromBorrowed = () => {
        dispatch(removeBorrowedBook(props.book.id));
    };
    return (
        <div className="borrowed-book-card">
            <img
                src={props.book.details.cover.src}
                alt={props.book.details.cover.alt}
                className="book-cover-static"
            />
            <div className="text-container">
                <h4 className="book-heading">{props.book.details.heading}</h4>
                <BookShortDetailsContainer book={props.book} />
                <ReturnButton
                    isButtonAvailable={
                        props.book.details.format.toLowerCase() === 'e-book'
                    }
                    onClick={removeFromBorrowed}
                />
            </div>
        </div>
    );
}
