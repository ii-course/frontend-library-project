import React, { useState, useEffect } from 'react';
import { getAuthorsByIds } from '../../store/repositories/authors_repository';
import { Button } from '../../buttons/button/button';
import { getFullName } from '../../helpers/strings';

export function getAuthorsLinkButtons(authors) {
    const buttons = [];
    authors.forEach((author) => {
        const button = (
            <Button
                key={author.id.toString()}
                style="dark-link"
                text={getFullName(author.name)}
                path={`/authors/${author.id}`}
            />);
        buttons.push(button);
    });
    return buttons;
}

export function AuthorsLinkButtons(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [authors, setAuthors] = useState([]);

    useEffect(() => {
        getAuthorsByIds(props.authorsId)
            .then(
                (result) => {
                    setAuthors(result);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.authorsId]);

    if (error) {
        return (
            <div>
              Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return (
        <>
            {getAuthorsLinkButtons(authors)}
        </>
    );
}
