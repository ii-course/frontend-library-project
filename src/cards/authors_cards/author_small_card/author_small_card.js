import React from 'react';
import './author_small_card.css';
import { HashLink } from 'react-router-hash-link';
import { getFullName } from '../../../helpers/strings';

export function AuthorSmallCard(props) {
    return (
        <div className="author-small-card" onClick={props.onClick} key={props.id}>
            <div className="author-small-card-img-wrapper">
                <img
                    src={props.author.img.src}
                    alt={props.author.img.alt}
                />
            </div>
            <HashLink to={`/authors/${props.author.id}`}>{getFullName(props.author.name)}</HashLink>
        </div>
    );
}
