import React from 'react';
import './full_screen_author_shortened_card.css';

export function FullScreenAuthorShortenedCard(props) {
    return (
        <div className="full-screen-author-shortened-card">
            <div className="img-container">
                <img src={props.img.src} alt={props.img.alt} />
            </div>
            <p>{props.biography}</p>
        </div>
    );
}
