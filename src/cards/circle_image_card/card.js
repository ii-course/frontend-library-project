import React from 'react';
import './card.css';

export function CircleImageCard(props) {
    return (
        <div className="circle-image-card circle-image-card-container">
            <div className="circle-image-card-container-for-img-and-header">
                <img src={props.img.src} alt={props.img.alt} />
                <h2 className="circle-image-card-text">{props.heading}</h2>
            </div>
            <p className="circle-image-card-text">{props.text}</p>
        </div>
    );
}
