import React from 'react';
import { Route } from 'react-router-dom';
import { HomePage } from './pages/home_page/home_page';
import { CatalogueMainPage } from './pages/catalogue_pages/catalogue_main_page/catalogue_main_page';
import { CatalogueBrowser } from './pages/catalogue_pages/catalogue_browser/catalogue_browser';
import { WhatsOnPage } from './pages/whats_on_page/whats_on_page';
import { AboutUsPage } from './pages/about_us_page/about_us_page';
import { TermsOfUsePage } from './pages/policy_page/terms_of_use_page/terms_of_use_page';
import { PrivacyPolicyPage } from './pages/policy_page/privacy_policy_page/privacy_policy_page';
import { ArticlePage } from './pages/article_page/article_page';
import { EventPage } from './pages/event_page/event_page';
import { BookPage } from './pages/book_page/book_page';
import { AuthorPage } from './pages/author_page/author_page';
import { AccountPageWrapper } from './pages/account_page/account_page_wrapper';
import SignUpPageWrapper from "./pages/sign_up_page/sign_up_page_wrapper";

export const PATHS_WITH_COMPONENTS = {
    '/': {
        component: HomePage,
        exact: true,
    },
    '/catalogue_main': {
        component: CatalogueMainPage,
    },
    '/catalogue_browser': {
        component: CatalogueBrowser,
    },
    '/whats_on': {
        component: WhatsOnPage,
    },
    '/about_us': {
        component: AboutUsPage,
    },
    '/sign_up': {
        component: SignUpPageWrapper,
    },
    '/terms_of_use': {
        component: TermsOfUsePage,
    },
    '/privacy_policy': {
        component: PrivacyPolicyPage,
    },
    '/articles/:id': {
        component: ArticlePage,
    },
    '/events/:type/:id': {
        component: EventPage,
    },
    '/books/:id': {
        component: BookPage,
    },
    '/authors/:id': {
        component: AuthorPage,
    },
    '/account': {
        component: AccountPageWrapper,
    },
};

export function getRoutes() {
    const routes = [];
    Object.entries(PATHS_WITH_COMPONENTS).forEach(([path, componentData]) => {
        const route = (
            <Route key={path} exact={componentData.exact} path={path} component={componentData.component} />
        );
        routes.push(route);
    });
    return routes;
}
