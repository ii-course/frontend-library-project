import React from 'react';
import './form_input_field.css';
import classNames from "classnames";

function formLabel(label, required) {
    return `${required ? '*' : ''} ${label}`;
}

export function FormInputField(props) {
    const label = <label htmlFor={props.id}>{formLabel(props.label, props.required)}</label>;
    const input = (
        <input
            className={classNames("form-input-field", props.className)}
            name={props.name ? props.name : ''}
            type={props.type}
            placeholder={props.placeholder}
            id={props.id}
            onChange={props.onChange ? props.onChange : null}
            min={props.min ? props.min : null}
            max={props.max ? props.max : null}
            required={props.required}
        />
    );
    return (
        <div className="form-input-field-container" key={props.id.toString()}>
            {props.label ? label : <></>}
            {input}
        </div>
    );
}
