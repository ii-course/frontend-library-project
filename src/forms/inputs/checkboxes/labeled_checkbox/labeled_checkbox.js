import React from 'react';
import { Field } from 'formik';
import { camelize } from '../../../../helpers/strings';

export function LabeledCheckbox(props) {
    const name = props.name ? props.name : camelize(props.label);
    const value = props.value ? props.value : props.label;
    const id = `filter-${name}-checkbox`;

    const onClick = () => {
        if (props.onClick) {
            props.onClick();
        }
    };

    return (
        <label>
            <Field
                type="checkbox"
                name={props.name}
                value={value} id={id}
                onClick={onClick}
                key={id}
            />
            {props.label}
        </label>
    );
}
