import {
    ArrayParam,
} from 'use-query-params';

export const queryParams = {
    section: ArrayParam,
    authors: ArrayParam,
    country: ArrayParam,
    language: ArrayParam,
    year: ArrayParam,
    publisher: ArrayParam,
};
