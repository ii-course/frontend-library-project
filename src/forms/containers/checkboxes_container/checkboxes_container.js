import React from 'react';
import { LabeledCheckbox } from '../../inputs/checkboxes/labeled_checkbox/labeled_checkbox';
import './checkboxes_container.css';

function getCheckboxesElements(checkboxesLabels, name) {
    const checkboxes = [];
    checkboxesLabels.forEach((label) => {
        const checkbox = <LabeledCheckbox name={name} label={label.toString()} />;
        checkboxes.push(checkbox);
    });
    return checkboxes;
}

function getCheckboxesWithValuesElements(checkboxesData, name) {
    const checkboxes = [];
    checkboxesData.forEach(({ label, value }) => {
        const checkbox = <LabeledCheckbox name={name} label={label.toString()} value={value.toString()} />;
        checkboxes.push(checkbox);
    });
    return checkboxes;
}

export function CheckboxesContainer(props) {
    return (
        <div className="checkboxes-container">
            {getCheckboxesElements(props.checkboxesLabels, props.name)}
        </div>
    );
}

export function CheckboxesContainerWithValues(props) {
    return (
        <div className="checkboxes-container">
            {getCheckboxesWithValuesElements(props.checkboxesData, props.name)}
        </div>
    );
}
