import React from 'react';
import { IconButtonWithMinimizeMaximizeState } from '../../../buttons/icon_buttons/buttons_with_state/button_with_minimize_maximize_state';
import './fieldset_container.css';
import { capitalize } from '../../../helpers/strings';

// @TODO: refactor FieldsetContainer

export class FieldsetContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isMinimized: this.props.isMinimized };
        this.toggleState = this.toggleState.bind(this);
    }

    toggleState() {
        const newState = this.state.isMinimized ? { isMinimized: false } : { isMinimized: true };
        this.setState(newState);
    }

    getFragmentToDisplay() {
        if (this.state.isMinimized) {
            return <></>;
        }
        return (
            <>
                {this.props.children}
            </>
        );
    }

    render() {
        return (
            <fieldset>
                <div
                    className="fieldset-legend-container" onClick={(event) => {
                        event.preventDefault();
                    }}
                >
                    <legend><h3>{capitalize(this.props.legend)}</h3></legend>
                    <IconButtonWithMinimizeMaximizeState
                        isMinimized={this.state.isMinimized}
                        onClick={this.toggleState}
                    />
                </div>
                {this.getFragmentToDisplay()}
            </fieldset>
        );
    }
}
