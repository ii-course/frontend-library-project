import React from 'react';
import { FormInputField } from '../../inputs/form_input_field/form_input_field';
import { LogInButton } from '../../../buttons/account_buttons/log_in_button/log_in_button';

export function LogInForm(props) {
    return (
        <form onSubmit={props.formik.handleSubmit}>
            <FormInputField
                type="text"
                id="login"
                placeholder="Login"
                name="login"
                onChange={props.formik.handleChange}
                value={props.formik.values.login}
                required
            />
            <FormInputField
                type="password"
                id="password"
                name="password"
                placeholder="Password"
                onChange={props.formik.handleChange}
                value={props.formik.values.password}
                required
            />
            <LogInButton type="submit"/>
        </form>
    );
}
