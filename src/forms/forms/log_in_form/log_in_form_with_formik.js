import React from 'react';
import { useFormik } from 'formik';
import { useDispatch } from 'react-redux';
import { LogInForm } from './log_in_form';
import { logIn } from '../../../store/store/user/user_slice';

export function LogInFormWithFormik() {
    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            login: '',
            password: '',
        },
        onSubmit: (values) => {
            dispatch(logIn({ login: values.login, password: values.password }));
        },
    });
    return <LogInForm formik={formik} />;
}
