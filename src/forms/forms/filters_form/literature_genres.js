export const fictionGenres = [
    'Action and adventure',
    'Children\'s',
    'Classic',
    'Crime',
    'Drama',
    'Fantasy',
    'Folklore',
    'Historical',
    'Horror',
    'Mystery',
    'Romance',
    'Science fiction',
    'Thriller',
    'Poetry',
];

export const nonFictionGenres = [
    'Popular Science',
    'History',
    'Biographies',
    'Travelling',
    'Academic texts',
    'Philosophy',
    'Journalism',
    'Guides and manuals',
    'Humor',
];
