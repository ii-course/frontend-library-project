import React, { useEffect, useState } from 'react';
import { FieldsetContainer } from '../../../../containers/fieldset_container/fieldset_container';
import { CheckboxesContainerWithValues } from '../../../../containers/checkboxes_container/checkboxes_container';
import { getAllAuthors } from '../../../../../store/repositories/authors_repository';
import { getFullNameReversed } from '../../../../../helpers/strings';

function sortByLabel(a, b) {
    if (a.label < b.label) {
        return -1;
    }
    if (a.label > b.label) {
        return 1;
    }
    return 0;
}

export function AuthorsNamesFieldset() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [checkboxesData, setCheckboxesData] = useState([]);

    useEffect(() => {
        getAllAuthors()
            .then(
                (authors) => {
                    const checkboxesDataToSet = authors.map((author) => ({
                        label: getFullNameReversed(author.name),
                        value: author.id,
                    }));
                    checkboxesDataToSet.sort(sortByLabel);
                    setCheckboxesData(checkboxesDataToSet);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, []);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return (
        <FieldsetContainer
            legend="Authors"
            key="authors"
            isMinimized>
            <CheckboxesContainerWithValues
                name="authorsId_like"
                checkboxesData={checkboxesData}
            />
        </FieldsetContainer>
    );
}
