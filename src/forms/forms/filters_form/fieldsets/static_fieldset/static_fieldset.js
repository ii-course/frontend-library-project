import React from 'react';
import { CheckboxesContainer } from '../../../../containers/checkboxes_container/checkboxes_container';
import { FieldsetContainer } from '../../../../containers/fieldset_container/fieldset_container';

export function StaticFieldset(props) {
    const checkboxes = (
        <CheckboxesContainer
            name={props.name}
            checkboxesLabels={props.checkboxesLabels}
        />
    );
    return (
        <FieldsetContainer
            legend={props.legend}
        >
            {checkboxes}
        </FieldsetContainer>
    );
}
