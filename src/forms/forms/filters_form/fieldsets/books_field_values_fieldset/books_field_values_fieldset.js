import React, { useEffect, useState } from 'react';
import { getAllBooksDetailValues } from '../../../../../store/repositories/books_repository';
import { FieldsetContainer } from '../../../../containers/fieldset_container/fieldset_container';
import { capitalize } from '../../../../../helpers/strings';
import { CheckboxesContainer } from '../../../../containers/checkboxes_container/checkboxes_container';

function BooksFieldValuesFieldset(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [fieldValues, setFieldValues] = useState([]);

    useEffect(() => {
        getAllBooksDetailValues(props.field.toLowerCase())
            .then(
                (detailValues) => {
                    setFieldValues(detailValues);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.field]);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return (
        <FieldsetContainer
            legend={capitalize(props.field)}
            key={props.field}
            isMinimized>
            <CheckboxesContainer
                name={props.field.toLowerCase()}
                checkboxesLabels={fieldValues}
            />
        </FieldsetContainer>
    );
}

export function BookFieldsValuesFieldsets() {
    const fieldsForFiltering = ['country', 'language', 'year', 'publisher'];
    const elements = fieldsForFiltering.map((field) => <BooksFieldValuesFieldset field={field} key="field"/>);
    return (
        <>
            {elements}
        </>
    );
}
