import qs from "query-string";
import {Formik} from "formik";
import React from "react";
import {FiltersForm} from "./filters_form";

export function FiltersFormWithFormik(props) {
    const initialValues = {
        section: [],
        genres_like: [],
        authorsId_like: [],
        country: [],
        language: [],
        year: [],
        publisher: [],
        format: [],
    };

    const onSubmit = (values) => {
        const query = qs.stringify(Object.assign(values, {_page: 1}));
        props.history.push({
            search: query,
        });
    };
    const onReset = () => {
        props.history.push({
            search: null,
        });
    };
    return (
        <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            onReset={onReset}
        >
            <FiltersForm/>
        </Formik>
    );
}
