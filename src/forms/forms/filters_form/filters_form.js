import React from 'react';
import './filter_form.css';
import '../form_with_checkboxes.css';
import {Form} from 'formik';
import {AuthorsNamesFieldset} from './fieldsets/authors_names_fieldset/authors_names_fieldset';
import {BookFieldsValuesFieldsets} from './fieldsets/books_field_values_fieldset/books_field_values_fieldset';
import {StaticFieldset} from './fieldsets/static_fieldset/static_fieldset';

const sections = ['Fiction', 'Non-fiction'];
const genres = [
    'Action and adventure',
    'Children\'s',
    'Classic',
    'Crime',
    'Drama',
    'Fantasy',
    'Folklore',
    'Historical',
    'Horror',
    'Mystery',
    'Romance',
    'Science fiction',
    'Thriller',
    'Poetry',
    'Popular Science',
    'History',
    'Biographies',
    'Travelling',
    'Academic texts',
    'Philosophy',
    'Journalism',
    'Guides and manuals',
    'Humor',
];

genres.sort();

const formatLabels = ['E-book', 'Paper book'];

export function FiltersForm() {
    return (
        <Form>
            <div className="buttons-container">
                <button type="submit" className="button-unfilled">Filter</button>
                <button type="reset" className="button-dark-link">Clear all</button>
            </div>
            <StaticFieldset legend="section" name="section" checkboxesLabels={sections} />
            <StaticFieldset legend="genres" name="genres_like" checkboxesLabels={genres} />
            <AuthorsNamesFieldset />
            <BookFieldsValuesFieldsets />
            <StaticFieldset legend="format" name="format" checkboxesLabels={formatLabels} />
        </Form>);
}

