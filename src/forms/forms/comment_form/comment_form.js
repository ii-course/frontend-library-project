import React from "react";
import {FormInputField} from "../../inputs/form_input_field/form_input_field";
import {Button} from "../../../buttons/button/button";
import './comment_form.css';

export function CommentForm(props) {
    const {values, handleChange, handleSubmit, errors} = props.formik;
    return (<form onSubmit={handleSubmit} className="comment-form">
        <FormInputField
            type="text"
            name="heading"
            id="heading"
            placeholder="Heading"
            onChange={handleChange}
            value={values.heading}
            required={true}/>
        <FormInputField
            type="number"
            name="stars"
            id="stars"
            placeholder="Stars"
            min="1"
            max="5"
            onChange={handleChange}
            value={values.heading}
            required={true}/>
        <textarea
            className="form-input-field"
            name="text"
            id="text"
            placeholder="Enter you comment..."
            onChange={handleChange}
            value={values.text}
            required={true}/>
        <div>
            <p className="validation-error">{errors.heading}</p>
            <p className="validation-error">{errors.text}</p>
        </div>
        <div className="buttons-container">
            <Button type="submit" style="filled" text="post"/>
        </div>
    </form>);
}
