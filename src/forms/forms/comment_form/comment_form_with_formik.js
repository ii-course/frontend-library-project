import {connect, useDispatch} from "react-redux";
import {useFormik} from "formik";
import React from "react";
import {CommentForm} from "./comment_form";
import {addComment} from "../../../store/store/comments/comments_slice";
import {mapStateToProps} from "../../../store/store/map_state_to_props";
import * as Yup from "yup";

const Schema = Yup.object().shape({
    heading: Yup
        .string()
        .max(50, 'Heading cannot be longer than 50 characters'),
    text: Yup
        .string()
        .max(10000, 'Text cannot be longer than 10000 characters'),
});


function CommentFormWithFormik(props) {
    const dispatch = useDispatch();
    const clearForm = (values) => {
        Object.keys(values).forEach((key) => {
            if (key !== 'bookId') {
                values[key] = '';
            }
        });
    };
    const formik = useFormik({
        initialValues: {
            heading: '',
            text: '',
            bookId: props.bookId
        },
        validationSchema: Schema,
        onSubmit: (values) => {
            dispatch(addComment(values));
            clearForm(values);
        },
    });
    return props.user ? <CommentForm formik={formik} /> :
        <div className="message">Log in to write comments</div>;
}

export default connect(mapStateToProps)(CommentFormWithFormik);
