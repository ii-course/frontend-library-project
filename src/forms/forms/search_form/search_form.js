import React from 'react';
import { SearchIconButtonWithPath } from '../../../buttons/icon_buttons/search_button/search_button';
import { FormInputField } from '../../inputs/form_input_field/form_input_field';
import './search_form.css';

export function SearchForm(props) {
    const {values, errors, handleChange, handleSubmit} = props.formik;
    return (
        <form className="search-form" onSubmit={handleSubmit}>
            <p className="validation-error">{errors.heading_like}</p>
            <div>
                <FormInputField
                    type="text"
                    placeholder="Search..."
                    name="heading_like"
                    id="search-input"
                    onChange={handleChange}
                    value={values.heading_like}
                />
                <SearchIconButtonWithPath type="submit" path={props.path} />
            </div>
        </form>
    );
}
