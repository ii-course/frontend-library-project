import { useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import React from 'react';
import qs from 'query-string';
import { SearchForm } from './search_form';
import * as Yup from "yup";

const Schema = Yup.object().shape({
    heading_like: Yup
        .string()
        .max(50, 'Search string cannot be longer than 50 characters')
        .trim()
        .min(1, 'Search string should not be empty'),
});

export function SearchFormWithFormik(props) {
    const ownHistory = useHistory();
    const history = props.history ? props.history : ownHistory;
    const formik = useFormik({
        initialValues: {
            heading_like: '',
        },
        validationSchema: Schema,
        onSubmit: (values) => {
            values.heading_like = values.heading_like.trim();
            const query = qs.stringify(Object.assign(values, {_page: 1}));
            history.goForward(); // fix?
            history.push({
                pathname: '/catalogue_browser',
                search: values.heading_like.length > 0 ? query : null, // mergeQueries(history.location.search, query),
            });
        },
    });
    return <SearchForm formik={formik} />;
}
