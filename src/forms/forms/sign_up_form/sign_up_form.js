import React from 'react';
import './sign_up_form.css';
import { FormInputField } from '../../inputs/form_input_field/form_input_field';
import { Button } from '../../../buttons/button/button';
import {getCurrentDateFromISOString} from "../../../helpers/dates";

export function SignUpForm(props) {
    const {values, errors, handleChange, handleSubmit} = props.formik;
    const name = (
        <>
            <div className="flex-row">
                <FormInputField
                    type="text"
                    id="first-name-sign-up"
                    label="First name"
                    name="firstName"
                    onChange={handleChange}
                    value={values.firstName}
                    required
                />
                <FormInputField
                    type="text"
                    id="last-name-sign-up"
                    label="Last name"
                    name="lastName"
                    onChange={handleChange}
                    value={values.lastName}
                    required
                />
            </div>
            <div>
                <p  className="validation-error">{errors.firstName}</p>
                <p  className="validation-error">{errors.lastName}</p>
            </div>
        </>
    );
    const birthdate = (
        <FormInputField
            type="date"
            id="birthdate-sign-up"
            label="Birthdate"
            max={getCurrentDateFromISOString()}
            name="birthdate"
            onChange={handleChange}
            value={values.birthdate}
            required
        />
    );
    const location = (
        <>
            <div className="flex-row">
                <FormInputField
                    type="text"
                    id="country-sign-up"
                    label="Country"
                    name="country"
                    onChange={handleChange}
                    value={values.country}
                />
                <FormInputField
                    type="text"
                    id="location-sign-up"
                    label="Location"
                    name="location"
                    onChange={handleChange}
                    value={values.location}
                />
            </div>
            <div>
                <p  className="validation-error">{errors.country}</p>
                <p  className="validation-error">{errors.location}</p>
            </div>
        </>
    );
    const email = (
        <FormInputField
            type="email"
            id="email-sign-up"
            label="E-mail"
            name="email"
            onChange={handleChange}
            value={values.email}
            required
        />
    );
    const password = (
        <>
            <div className="flex-row">
                <FormInputField
                    type="password"
                    id="password-sign-up"
                    label="Password"
                    name="password"
                    onChange={handleChange}
                    value={values.password}
                    required
                />
                <FormInputField
                    type="password"
                    id="password-confirm-sign-up"
                    label="Confirm password"
                    name="confirmPassword"
                    onChange={handleChange}
                    value={values.confirmPassword}
                    required
                />
            </div>
            <div>
                <p  className="validation-error">{errors.password}</p>
                <p  className="validation-error">{errors.confirmPassword}</p>
            </div>
        </>
    );
    const agreementConfirmation = (
        <div className="agreement-confirmation">
            <input type="checkbox"
                id="agreement-confirm-sign-up"
                required/>
            <label htmlFor="agreement-confirm-sign-up">
            By continuing, I agree with Terms of Service and Privacy Policy
            </label>
        </div>
    );
    return (
        <form className="sign-up-form" onSubmit={handleSubmit}>
            <div className="main-part">
                {name}
                {birthdate}
                {location}
                {email}
                {password}
            </div>
            {agreementConfirmation}
            <Button type="submit" style="filled" text="Sign up" />
        </form>
    );
}
