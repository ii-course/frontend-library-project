import {useDispatch} from "react-redux";
import {useFormik} from "formik";
import React from "react";
import {SignUpForm} from "./sign_up_form";
import * as Yup from 'yup';
import {signUp} from "../../../store/store/user/user_slice";

const Schema = Yup.object().shape({
    firstName: Yup
        .string()
        .max(50, 'First name cannot be longer than 50 characters')
        .trim()
        .min(1, 'First name should not be empty'),
    lastName: Yup
        .string()
        .max(50, 'Last name cannot be longer than 50 characters')
        .trim()
        .min(1, 'Last name should not be empty'),
    country: Yup
        .string()
        .max(20, 'Country name cannot be longer than 20 characters'),
    location: Yup
        .string()
        .max(50, 'Location name cannot be longer than 50 characters'),
    password: Yup
        .string()
        .min(6, 'Password length should be at least 6 characters'),
    confirmPassword: Yup.string().when("password", {
        is: val => (!!(val && val.length > 0)),
        then: Yup.string().oneOf(
            [Yup.ref("password")],
            "Passwords do not match"
        )
    })
});

export function SignUpFormWithFormik() {
    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            birthdate: '',
            country: '',
            location: '',
            email: '',
            password: '',
            confirmPassword: ''
        },
        validationSchema: Schema,
        onSubmit: (values) => {
            dispatch(signUp(values));
        },
    });
    return <SignUpForm formik={formik} />;
}
