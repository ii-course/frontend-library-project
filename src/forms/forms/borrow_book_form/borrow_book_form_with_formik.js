import React from 'react';
import { useFormik } from 'formik';
import { useDispatch } from 'react-redux';
import { BorrowBookForm } from './borrow_book_form';
import { orderBook } from '../../../store/store/user/user_slice';

export function BorrowBookFormWithFormik(props) {
    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            receivingDate: null,
        },
        onSubmit: (values) => {
            console.log(values);
            dispatch(orderBook({ bookId: props.bookId, receivingDate: values.receivingDate }));
            props.onSubmit();
        },
    });
    return <BorrowBookForm formik={formik} />;
}
