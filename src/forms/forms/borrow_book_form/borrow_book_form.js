import React from 'react';
import {FormInputField} from '../../inputs/form_input_field/form_input_field';
import {Button} from '../../../buttons/button/button';
import {getCurrentDateFromISOString} from "../../../helpers/dates";

export function BorrowBookForm(props) {
    return (
        <form onSubmit={props.formik.handleSubmit} className="borrow-book-form">
            <FormInputField
                type="date"
                label="Date"
                id="receivingDate"
                name="receivingDate"
                onChange={props.formik.handleChange}
                value={props.formik.values.receivingDate}
                required
                min={getCurrentDateFromISOString()}
            />
            <Button type="submit" style="filled" text="submit" />
        </form>
    );
}
