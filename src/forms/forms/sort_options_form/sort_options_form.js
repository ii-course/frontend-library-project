import React  from 'react';
import './sort_options_form.css';
import {Form, Field} from 'formik';
import '../../../buttons/button/button.css';
import '../form_with_checkboxes.css';

export function SortOptionsForm() {
    const getInputWithLabel = (label, required = false) => {
        const id = `sort-by-${label.toLowerCase().split(' ').join('-')}`;
        return (
            <label>
                <Field
                    type="radio" name="_sort" id={id} required={required}
                    value={label.toLowerCase()}
                />
                {label}
            </label>
        );
    };

    const getAllInputsWithLabels = () => {
        const sortByOptions = [
            'Name',
            'Year',
            'Availability',
        ];
        const inputsWithLabels = [];
        sortByOptions.forEach((option, index) => {
            const isFirst = index === 0;
            const inputWithLabel = getInputWithLabel(option, isFirst);
            inputsWithLabels.push(inputWithLabel);
        });
        return inputsWithLabels;
    };

    const inputsWithLabels = getAllInputsWithLabels();
    return (
        <Form className="sort-options-form form-with-checkboxes">
            <div className="radio-buttons-container">
                <div className="options-container">
                    <span>Sort by: </span>
                    {inputsWithLabels}
                </div>
                <div className="options-container">
                    <span>Sort order: </span>
                    <label>
                        <Field
                            type="radio" name="_order"
                            value="asc"
                            key="sortOrderAscButton"
                            required={true}
                        />
                        ASC
                    </label>
                    <label>
                        <Field
                            type="radio" name="_order"
                            value="desc"
                            key="sortOrderDescButton"
                        />
                        DESC
                    </label>
                </div>
            </div>
            <div className="submit-reset-buttons-container">
                <button type="submit" className="button-dark-link">
                    Submit
                </button>
                <button type="reset" className="button-dark-link">
                    Reset
                </button>
            </div>
        </Form>
    );
}
