import { useHistory } from 'react-router-dom';
import React from 'react';
import qs from 'query-string';
import {Formik} from 'formik';
import { SortOptionsForm } from './sort_options_form';

function replace(mainQuery, queryToAdd) {
    if (!mainQuery) {
        return queryToAdd;
    }
    const mainQueryParsed = qs.parse(mainQuery);
    const queryToAddParsed = qs.parse(queryToAdd);
    return qs.stringify(Object.assign(mainQueryParsed, queryToAddParsed));
}

export function SortOptionsFormWithFormik(props) {
    const ownHistory = useHistory();
    const history = props.history ? props.history : ownHistory;

    const initialValues = {
        _sort: '',
        _order: '',
    };
    const onSubmit = (values) => {
        const query = qs.stringify(values);
        history.goForward();
        history.push({
            pathname: '/catalogue_browser',
            search: replace(history.location.search, query),
        });
    };
    const onReset = () => {
        history.goForward();
        history.push({
            pathname: '/catalogue_browser',
            search: null,
        });
    };
    return <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        onReset={onReset}
    >
        <SortOptionsForm />
    </Formik>;
}
