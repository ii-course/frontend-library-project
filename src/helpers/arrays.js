export function removeValue(array, value) {
    return array.filter((element) => element !== value);
}
