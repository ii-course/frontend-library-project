export function getFirstNSentences(string, count) {
    const sentences = string.split('. ');
    const endIndex = count < string.length ? count : string.length;
    return `${sentences.slice(0, endIndex).join('. ')}...`;
}

export function getFirstSentence(string) {
    return getFirstNSentences(string, 1);
}

export function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export function getFullName(name) {
    return `${capitalize(name.first)} ${capitalize(name.last)}`;
}

export function getFullNameReversed(name) {
    return `${capitalize(name.last)} ${capitalize(name.first)}`;
}

export function camelize(string) {
    return string.replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => (index === 0 ? word.toLowerCase() : word.toUpperCase())).replace(/\s+/g, '');
}

export function removeLastCharacter(string) {
    return string.slice(0, -1);
}
