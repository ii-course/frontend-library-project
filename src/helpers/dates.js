export function formatDate(dateString) {
    return new Date(dateString).toLocaleDateString('en-GB');
}

export function getCurrentDateFromISOString() {
    return new Date().toISOString().substring(0, 10);
}
