import React from 'react';
import { Button } from '../../buttons/button/button';
import './page_button.css';

export class PageButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = { style: this.props.style };
        this.setState = this.setState.bind(this);
        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        // this.setState({style: 'current-link'}); // @TODO
        const pageNumber = Number.parseInt(this.props.text);
        this.props.onClick(pageNumber);
    }

    render() {
        return <Button style={this.state.style} text={this.props.text} onClick={this.onClick} />;
    }
}
