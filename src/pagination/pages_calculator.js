export function getPagesTotal(cardsTotal, cardsPerPage) {
    return Math.ceil(cardsTotal / cardsPerPage);
}
