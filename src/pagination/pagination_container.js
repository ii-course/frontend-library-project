import React from 'react';
import { PageButton } from './page_button/page_button';
import './pagination_container.css';

// @TODO: refactor PaginationContainer

export class PaginationContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = { currentPage: this.props.currentPage };
        this.setPage = this.setPage.bind(this);
    }

    getEllipses(key = 0) {
        return <span key={`ellipses${key}`}>...</span>;
    }

    getPageButton(pageNumber) {
        return (
            <PageButton
                key={pageNumber.toString()}
                text={pageNumber.toString()}
                style={this.props.style}
                onClick={this.setPage}
            />
        );
    }

    getFirstButton() {
        return this.getPageButton(1);
    }

    getLastButton() {
        const { pagesTotal } = this.props;
        return this.getPageButton(pagesTotal);
    }

    getButtonsFromTo(from, to) {
        const buttons = [];
        for (let i = from; i <= to; i += 1) {
            const button = this.getPageButton(i);
            buttons.push(button);
        }
        return buttons;
    }

    getElementsForBeginning() {
        const elementsToDisplay = [];
        const { pagesTotal } = this.props;
        const ellipses = this.getEllipses();
        const lastButton = this.getLastButton();
        elementsToDisplay.push(this.getButtonsFromTo(1, pagesTotal < 3 ? pagesTotal : 3));
        if (pagesTotal > 3) {
            elementsToDisplay.push(ellipses, lastButton);
        }
        return elementsToDisplay;
    }

    getElementsForMiddle() {
        const elementsToDisplay = [];
        const { currentPage } = this.state;
        const firstButton = this.getFirstButton();
        const lastButton = this.getLastButton();
        elementsToDisplay.push(
            firstButton,
            this.getEllipses(0),
            this.getButtonsFromTo(currentPage - 1, currentPage + 1),
            this.getEllipses(1),
            lastButton,
        );
        return elementsToDisplay;
    }

    getElementsForTheEnd() {
        const elementsToDisplay = [];
        const { pagesTotal } = this.props;
        const ellipses = this.getEllipses();
        const firstButton = this.getFirstButton();
        elementsToDisplay.push(
            firstButton,
            ellipses,
            this.getButtonsFromTo(pagesTotal - 2, pagesTotal),
        );
        return elementsToDisplay;
    }

    getElementsToDisplay() {
        // 1 2 3 ... 64
        // 1 ... 4 5 6 ... 64
        // 1 ... 62 63 64
        const { currentPage } = this.state;
        const { pagesTotal } = this.props;
        const pageIsInBeginning = currentPage < 3 || pagesTotal < 4;
        const pageIsInMiddle = currentPage >= 3 && currentPage < pagesTotal - 1;
        if (pageIsInBeginning) {
            return this.getElementsForBeginning();
        }
        if (pageIsInMiddle) {
            return this.getElementsForMiddle();
        }
        return this.getElementsForTheEnd();
    }

    setPage(pageNumber) {
        this.setState({ currentPage: pageNumber });
        this.props.onClick(pageNumber);
    }

    render() {
        return (
            <div className="pagination-container">
                {this.getElementsToDisplay()}
            </div>
        );
    }
}
