import React, { useState, useEffect } from 'react';
import { CompactCommentCard } from '../../cards/comments_cards/compact_comment_card/compact_comment_card';
import './compact_comment_cards_container.css';
import { getCommentsByBookId } from '../../store/repositories/comments_repository';
import {connect} from "react-redux";
import {mapStateToProps} from "../../store/store/map_state_to_props";

function CompactCommentCardsContainer(props) {
    const NUMBER_OF_CARDS_TO_DISPLAY = 4;
    const cards = [];
    for (let i = 0; i < NUMBER_OF_CARDS_TO_DISPLAY && i < props.comments.length; i += 1) {
        const comment = props.comments[i];
        const card = (
            <CompactCommentCard
                stars={comment.stars}
                heading={comment.heading}
                text={comment.text}
                key={i.toString()}
            />
        );
        cards.push(card);
    }
    return (
        <div className="compact-comment-cards-container">
            {cards.length > 0 ? cards : <div className="message message-light">The book has no comments yet...</div>}
        </div>
    );
}

function CompactCommentCardsContainerWrapper(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [comments, setComments] = useState([]);

    useEffect(() => {
        getCommentsByBookId(props.bookId)
            .then(
                (comments) => {
                    setComments(comments);
                    setIsLoaded(true);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                },
            );
    }, [props.bookId, props.addedComments]);

    if (error) {
        return (
            <div>
              Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <CompactCommentCardsContainer comments={comments} />;
}

export default connect(mapStateToProps)(CompactCommentCardsContainerWrapper);
