import React from 'react';
import './text_container_with_background_image.css';

export function TextContainerWithBackgroundImage(props) {
    return (
        <div className="image-wrapper">
            <img
                className="image" src={props.img.src}
                alt={props.img.alt}
            />
            <div className="content-wrapper image-text-content-wrapper">
                {props.textChild}
            </div>
        </div>
    );
}
