import React from 'react';
import './container.css';
import '../../scss/wrapper.css';

export function FilledContainerWithButton(props) {
    return (
        <div className="filled-container-with-button">
            <div className="content-wrapper filled-container-with-button-content-container">
                <h1>{props.heading}</h1>
                <p>{props.text}</p>
                {props.button}
            </div>
        </div>
    );
}
