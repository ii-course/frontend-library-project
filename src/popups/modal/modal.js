import React from 'react';
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import { CloseButton } from '../../buttons/icon_buttons/close_button/close_button';
import './modal.css';

export function Modal(props) {
    return (
        <Popup trigger={props.trigger} modal nested>
            {(close) => (
                <div className="modal">
                    <CloseButton onClick={close} />
                    {props.content}
                </div>
            )}
        </Popup>
    );
}
