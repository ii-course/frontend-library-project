import React from 'react';
import Popup from 'reactjs-popup';
import './log_in_popup.css';
import 'reactjs-popup/dist/index.css';
import { SignUpButton } from '../../buttons/account_buttons/sign_up_button/sign_up_button';
import { CloseButton } from '../../buttons/icon_buttons/close_button/close_button';
import { LogInFormWithFormik } from '../../forms/forms/log_in_form/log_in_form_with_formik';
import '../modal/modal.css';

export function LogInPopup(props) {
    return (
        <Popup trigger={props.trigger} modal nested>
            {(close) => (
                <div className="modal">
                    <CloseButton onClick={close} />
                    <div className="log-in-popup-content">
                        <LogInFormWithFormik onSubmit={close} />
                        <div className="additional">
                            <p className="separator">Do not have an account?</p>
                            <SignUpButton style="unfilled" onClick={close} />
                        </div>
                    </div>
                </div>
            )}
        </Popup>
    );
}
