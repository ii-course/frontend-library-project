import React from 'react';
import Popup from 'reactjs-popup';
import { CloseButton } from '../../buttons/icon_buttons/close_button/close_button';
import 'reactjs-popup/dist/index.css';
import '../modal/modal.css';
import { BorrowBookFormWithFormik } from '../../forms/forms/borrow_book_form/borrow_book_form_with_formik';
import './borrow_book_popup.css';

export function BorrowBookPopup(props) {
    return (
        <Popup trigger={props.trigger} modal nested>
            {(close) => (
                <div className="modal">
                    <CloseButton onClick={close} />
                    <div>
                        <p>
                        Please, select the date when it is
                        convenient for you to pick the book up:
                        </p>
                        <BorrowBookFormWithFormik onSubmit={close} bookId={props.bookId} />
                    </div>
                </div>
            )}
        </Popup>
    );
}
