export function formStates(windowWidthWithCardsPerRow) {
    const states = [];
    Object.entries(windowWidthWithCardsPerRow).forEach(([width, cardsPerRow]) => {
        const state = {
            windowMaxWidth: Number.parseInt(width),
            state: {
                cardsPerRow,
            },
        };
        states.push(state);
    });
    return states;
}
