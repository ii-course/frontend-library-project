import React, {useState, useEffect} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import '../../scss/icons.css';
import '../../scss/wrapper.css';
import classNames from 'classnames';
import './gallery.css';
import { faCircle as farCircle } from '@fortawesome/free-regular-svg-icons';
import { faCircle as fasCircle } from '@fortawesome/free-solid-svg-icons';
import { ArrowButtonLeft, ArrowButtonRight } from '../../buttons/icon_buttons/arrow_button/arrow_button';

export function SliderGallery(props) {
    const [currentPageNum, setCurrentPageNum] = useState(0);
    const NUM_OF_CIRCLES = 4;

    useEffect(() => {
        const interval = setInterval(() => {
            setNextPage();
        }, 5000);

        return () => {
            clearInterval(interval);
        };
    }, []);

    const getCircles = () => {
        const circlesElements = [];
        const FILLED_CIRCLE = fasCircle;
        const UNFILLED_CIRCLE = farCircle;
        for (let i = 0; i < NUM_OF_CIRCLES; i += 1) {
            const isOpened = currentPageNum % NUM_OF_CIRCLES === i;
            const icon = isOpened ? FILLED_CIRCLE : UNFILLED_CIRCLE;
            const dot = <FontAwesomeIcon icon={icon}
                color="white"
                className="circleIcon"
                key={i.toString()}
                onClick={() => {setCurrentPageNum(i);}}/>;
            circlesElements.push(dot);
        }
        return circlesElements;
    };

    const getPagesCount = () => {
        return props.pages.length;
    };

    const setNextPage = () => {
        const pagesCount = getPagesCount();
        let nextPage = currentPageNum + 1;
        if (nextPage >= pagesCount) {
            nextPage = 0;
        }
        setCurrentPageNum(nextPage);
    };

    const setPrevPage = () => {
        const pagesCount = getPagesCount();
        let prevPage = currentPageNum - 1;
        if (prevPage < 0) {
            prevPage = pagesCount - 1;
        }
        setCurrentPageNum(prevPage);
    };

    const circlesElements = getCircles();
    const curPage = props.pages[currentPageNum];
    const classes = classNames('gallery', props.className);
    return (
        <div className={classes}>
            <img src={curPage.img.src} alt={curPage.img.alt} className="gallery-img" />
            <div className="gallery-buttons-text-container content-wrapper">
                <ArrowButtonLeft color="white" onClick={setPrevPage} />
                <div className="gallery-central-buttons-text-container">
                    <div>
                        <h1>{curPage.heading}</h1>
                        <p>{curPage.text}</p>
                    </div>
                    <div className="gallery-circles">
                        {circlesElements}
                    </div>
                </div>
                <ArrowButtonRight color="white" onClick={setNextPage} />
            </div>
        </div>
    );
}
