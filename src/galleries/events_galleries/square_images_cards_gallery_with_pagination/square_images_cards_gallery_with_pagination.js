import React from 'react';
import { GalleryWithPagination } from '../../gallery_with_pagination/template_gallery_with_pagination/gallery_with_pagination';
import { EventCardWithSquareImage } from '../../../cards/events_cards/event_card_with_image/event_card_with_square_image/event_card_with_square_image';

function getCardsElements(cards, button) {
    const cardsElements = [];
    cards.forEach((card) => {
        const cardElement = (
            <EventCardWithSquareImage
                img={card.img}
                heading={card.heading}
                date={card.date}
                button={button}
                id={card.id}
                key={card.id}
            />
        );
        cardsElements.push(cardElement);
    });
    return cardsElements;
}

export function SquareImageCardsGalleryWithPagination(props) {
    const cardsElements = getCardsElements(props.events, props.button);
    return (<GalleryWithPagination pagination={props.button}
        cards={cardsElements}
        setPage={props.setPage}
        pagesTotal={props.pagesTotal}/>);
}
