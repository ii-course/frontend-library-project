import React, { useEffect, useState } from 'react';
import {SquareImageCardsGalleryWithPagination} from "./square_images_cards_gallery_with_pagination";
import {getPagesTotal} from "../../../pagination/pages_calculator";
import {getAllEventsCount, getEvents} from "../../../store/repositories/events_repository";

export function ExhibitionsGalleryWithPagination() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [events, setEvents] = useState([]);
    const [page, setPage] = useState(1);
    const [pagesTotal, setPagesTotal] = useState(1);

    const PER_PAGE = 8;
    const EVENT_TYPE = 'exhibitions';

    useEffect(() => {
        getEvents(EVENT_TYPE, page, PER_PAGE)
            .then(
                (eventsFetched) => {
                    setEvents(eventsFetched);
                    return getAllEventsCount(EVENT_TYPE);
                },
            )
            .then(
                (count) => {
                    setPagesTotal(getPagesTotal(count, PER_PAGE));
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                });
    }, [page]);

    if (error) {
        return (
            <div>
                Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <SquareImageCardsGalleryWithPagination events={events}
        setPage={(pageNum) => {
            setPage(pageNum);
        }}
        pagesTotal={pagesTotal}
        button={{ style: 'light-link' }}/>;
}
