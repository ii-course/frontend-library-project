import React, { useState, useEffect } from 'react';
import { AuthorsGallery } from '../authors_gallery/authors_gallery';
import {
    getAllAuthorsByBooksParams,
} from '../../../store/repositories/authors_repository';

export function AuthorsGalleryWithFetching(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [authors, setAuthors] = useState([]);

    useEffect(() => {
        getAllAuthorsByBooksParams(props.booksParams)
            .then(
                (authorsFetched) => {
                    setAuthors(authorsFetched);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.booksParams]);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <AuthorsGallery authors={authors} />;
}
