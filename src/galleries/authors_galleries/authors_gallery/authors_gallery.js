import React from 'react';
import { CardsScrollGallery } from '../../cards_scroll_gallery/cards_scroll_gallery';
import { AuthorSmallCard } from '../../../cards/authors_cards/author_small_card/author_small_card';

export function AuthorsGallery(props) {
    const formCards = () => {
        const cards = [];
        props.authors.forEach((author) => {
            const card = (
                <AuthorSmallCard
                    author={author}
                    key={author.id.toString()}
                />
            );
            cards.push(card);
        });
        return cards;
    };

    return <CardsScrollGallery cards={formCards()} />;
}
