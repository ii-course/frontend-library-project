import React, { useEffect, useState } from 'react';
import { getAllBooksByIds } from '../../../store/repositories/books_repository';
import { BooksScrollGallery } from '../books_scroll_gallery/books_scroll_gallery';
import { BooksGroupedByDateGallery } from '../books_grouped_by_date_gallery/books_grouped_by_date_gallery';

function getFilledGallery(galleryType, books) {
    if (galleryType === 'borrowed') {
        return <BooksGroupedByDateGallery books={books} />;
    }
    return <BooksScrollGallery books={books} />;
}

export function BooksGalleryWithFetchById(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [books, setBooks] = useState([]);

    useEffect(() => {
        getAllBooksByIds(props.booksId)
            .then(
                (books) => {
                    setBooks(books);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.booksId]);

    if (error) {
        return (
            <div>
              Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return getFilledGallery(props.galleryType, books);
}
