import React, { useEffect, useState } from 'react';
import { BorrowedBookCard } from '../../../cards/book_cards/borrowed_book_card/borrowed_book_card';
import './books_grouped_by_date_gallery.css';
import { getAllBooksByIds } from '../../../store/repositories/books_repository';

function compareBooksByDate(a, b, dateField) {
    if (a[dateField] < b[dateField]) {
        return -1;
    }
    if (a[dateField] > b[dateField]) {
        return 1;
    }
    return 0;
}

function getDateFormatted(dateString) {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(dateString).toLocaleDateString('en-GB', options);
}

function getBooksGroupedByDate(books, dateField) {
    books.sort((a, b) => compareBooksByDate(a, b, dateField));
    let currentDate;
    const grouped = {};
    books.forEach((book) => {
        if (currentDate !== book[dateField]) {
            currentDate = book[dateField];
        }
        const key = getDateFormatted(currentDate);
        const groupNotInitialized = !grouped[key];
        if (groupNotInitialized) {
            grouped[key] = [];
        }
        grouped[key].push(book);
    });
    return grouped;
}

function getBooksCards(books) {
    const cards = [];
    books.forEach((book) => {
        cards.push(<BorrowedBookCard key={book.id.toString()} book={book} />);
    });
    return cards;
}

function getBooksSection(date, books, labelBeforeDate) {
    return (
        <section key={date}>
            <h3>{`${labelBeforeDate} ${date}`}</h3>
            {getBooksCards(books)}
        </section>
    );
}

function getAllBooksSections(booksGroupedByEndDate, labelBeforeDate) {
    const sections = [];
    Object.entries(booksGroupedByEndDate).forEach(([date, books]) => {
        const section = getBooksSection(date, books, labelBeforeDate);
        sections.push(section);
    });
    return sections;
}

function getBooksSectionsGroupedByDate(books, dateField, labelBeforeDate) {
    const booksGroupedByEndDate = getBooksGroupedByDate(books, dateField);
    return getAllBooksSections(booksGroupedByEndDate, labelBeforeDate);
}

export function BooksGroupedByDateGallery(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [books, setBooks] = useState([]);

    useEffect(() => {
        const booksId = props.books.map((book) => book.bookId);
        getAllBooksByIds(booksId)
            .then(
                (booksData) => {
                    const booksToSet = booksData.map((bookData) => {
                        const dates = props.books.find((book) => book.bookId === bookData.id);
                        return Object.assign(bookData, dates);
                    });
                    setBooks(booksToSet);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.books]);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return (
        <div className="books_grouped_by_date_gallery">
            {getBooksSectionsGroupedByDate(books, props.dateField, props.labelBeforeDate)}
        </div>
    );
}
