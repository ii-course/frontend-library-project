import React, { useState, useEffect } from 'react';
import { getRelatedBooks } from '../../../../store/repositories/books_repository';
import { BooksScrollGallery } from '../books_scroll_gallery';

export function RelatedBooksGallery(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [books, setBooks] = useState([]);

    useEffect(() => {
        getRelatedBooks(props.bookId)
            .then(
                (booksFetched) => {
                    setBooks(booksFetched);
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.bookId]);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <BooksScrollGallery books={books} />;
}
