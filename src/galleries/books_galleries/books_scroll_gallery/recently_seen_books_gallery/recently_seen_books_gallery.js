import React from 'react';
import { BooksGalleryWithFetchById } from '../../books_gallery_with_fetch_by_id/books_gallery_with_fetch_by_id';
import {connect} from "react-redux";
import {mapStateToProps} from "../../../../store/store/map_state_to_props";

function RecentlySeenBooksGallery(props) {
    return <BooksGalleryWithFetchById booksId={props.recentlySeenBooksId} galleryType="scroll" />;
}

export default connect(mapStateToProps)(RecentlySeenBooksGallery);
