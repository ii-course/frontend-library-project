import React from 'react';
import { CardsScrollGallery } from '../../cards_scroll_gallery/cards_scroll_gallery';
import { formBooksCards } from '../books_cards_creator';

export function BooksScrollGallery(props) {
    return <CardsScrollGallery cards={formBooksCards(props.books)} />;
}
