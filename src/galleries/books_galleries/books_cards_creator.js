import React from 'react';
import { CompactBookCard } from '../../cards/book_cards/compact_book_card/compact_book_card';

export function formBooksCards(books) {
    const cards = [];
    books.forEach((book) => {
        const card = <CompactBookCard book={book} key={book.id.toString()} />;
        cards.push(card);
    });
    return cards;
}
