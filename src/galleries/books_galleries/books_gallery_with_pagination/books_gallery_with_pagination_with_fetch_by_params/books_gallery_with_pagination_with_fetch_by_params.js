import React, {useEffect, useState} from 'react';
import {BooksGalleryWithPagination} from '../books_gallery_with_pagination';
import {getAllBooksByParams, getBooksByParamsCount} from '../../../../store/repositories/books_repository';
import {getPagesTotal} from "../../../../pagination/pages_calculator";

export function BooksGalleryWithPaginationWithFetchByParams(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [books, setBooks] = useState([]);
    const [page, setPage] = useState(1);
    const [pagesTotal, setPagesTotal] = useState(1);

    const PER_PAGE = props.perPage ? props.perPage : 12;

    useEffect(() => {
        const params = Object.assign({
            _page: page,
            _limit: PER_PAGE,
        }, props.params);
        getAllBooksByParams(params, props.emptyOption)
            .then(
                (booksFetched) => {
                    setBooks(booksFetched);
                    return getBooksByParamsCount(params);
                })
            .then(
                (count) => {
                    setPagesTotal(getPagesTotal(count, PER_PAGE));
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                });
    }, [props.params, page]);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    }
    if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return (<BooksGalleryWithPagination books={books}
        pagesTotal={pagesTotal}
        setPage={(pageNum) => {
            setPage(pageNum);
        }}/>);
}
