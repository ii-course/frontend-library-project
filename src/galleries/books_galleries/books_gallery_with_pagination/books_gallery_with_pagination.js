import React from 'react';
import { GalleryWithPagination } from '../../gallery_with_pagination/template_gallery_with_pagination/gallery_with_pagination';
import { formBooksCards } from '../books_cards_creator';
import './books_gallery_with_pagination.css';

export function BooksGalleryWithPagination(props) {
    return (
        <GalleryWithPagination
            cards={formBooksCards(props.books)}
            pagesTotal={props.pagesTotal}
            pagination={{ style: 'dark-link' }}
            setPage={props.setPage}
        />
    );
}
