import React, { useEffect, useState } from 'react';
import { BooksGalleryWithPagination } from '../books_gallery_with_pagination';
import {getBooksByAuthor, getBooksByAuthorCount} from '../../../../store/repositories/books_repository';
import {getPagesTotal} from "../../../../pagination/pages_calculator";

export function AuthorBooksGalleryWithPagination(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [books, setBooks] = useState([]);
    const [page, setPage] = useState(1);
    const [pagesTotal, setPagesTotal] = useState(1);

    const PER_PAGE = 40;

    useEffect(() => {
        getBooksByAuthor(props.authorId, page, PER_PAGE)
            .then(
                (booksFetched) => {
                    setBooks(booksFetched);
                    return getBooksByAuthorCount(props.authorId);
                })
            .then(
                (count) => {
                    setPagesTotal(getPagesTotal(count, PER_PAGE));
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                },
            );
    }, [props.authorId, page]);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <BooksGalleryWithPagination books={books}
        pagesTotal={pagesTotal}
        setPage={(pageNum) => {
            setPage(pageNum);
        }}/>;
}
