import React from 'react';
import { CompactBookCard } from '../../../cards/book_cards/compact_book_card/compact_book_card';
import { ArrowButtonRight } from '../../../buttons/icon_buttons/arrow_button/arrow_button';
import './books_gallery_short.css';
import { DEFAULT_GALLERY_DIMENSIONS_CALCULATOR } from '../../gallery_dimensions_calculator/gallery_dimensions_calculator';


export class BooksGalleryShort extends React.Component {
    constructor(props) {
        super(props);
        this.state = DEFAULT_GALLERY_DIMENSIONS_CALCULATOR.calculateDimensions();
        this.handleResize = this.handleResize.bind(this);
    }

    handleResize() {
        const newState = DEFAULT_GALLERY_DIMENSIONS_CALCULATOR.calculateDimensions();
        this.setState(newState);
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }

    getBookCards(books) {
        const bookCards = [];
        for (let i = 0; i < this.state.cardsPerRow && i < books.length; i += 1) {
            const book = books[i];
            const bookCard = (
                <CompactBookCard
                    key={book.id}
                    book={book}
                />
            );
            bookCards.push(bookCard);
        }
        return bookCards;
    }

    render() {
        const bookCards = this.getBookCards(this.props.books);
        const lastIndex = bookCards.length - 1;
        bookCards[lastIndex] = (
            <div className="last-book" key="lastBook">
                <div className="last-book-wrapper">
                    <ArrowButtonRight color="white" path={this.props.path} />
                </div>
                {bookCards[lastIndex]}
            </div>
        );
        return (
            <div className="books-gallery-short">
                {bookCards}
            </div>
        );
    }
}

export function BooksGalleryShortWrapper(props) {
    if (props.books && props.books.length > 0) {
        return <BooksGalleryShort books={props.books} path={`/books/${props.genre}`} />;
    }
    return <div>No books found</div>;
}
