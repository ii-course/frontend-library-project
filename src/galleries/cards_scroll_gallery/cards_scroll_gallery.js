import React, { useState } from 'react';
import { ArrowButtonLeft, ArrowButtonRight } from '../../buttons/icon_buttons/arrow_button/arrow_button';
import './cards_scroll_gallery.css';

export function CardsScrollGallery(props) {
    const [cardsScrolledLeft, setCardsScrolledLeft] = useState(0);

    const getCardsToDisplay = () => props.cards.slice(cardsScrolledLeft);

    const isLeftButtonDisabled = () => cardsScrolledLeft <= 0;

    const isRightButtonDisabled = () => cardsScrolledLeft >= props.cards.length - 1;

    const moveLeft = () => {
        setCardsScrolledLeft((prevValue) => prevValue - 1);
    };

    const moveRight = () => {
        setCardsScrolledLeft((prevValue) => prevValue + 1);
    };

    return props.cards.length <= 0 ? <div className="nothing-found-message">Nothing found</div> : (
        <div className="cards-scroll-gallery">
            <ArrowButtonLeft
                color="black"
                onClick={moveLeft}
                disabled={isLeftButtonDisabled()}
            />
            <div className="cards-container">
                {getCardsToDisplay()}
            </div>
            <ArrowButtonRight
                color="black"
                onClick={moveRight}
                disabled={isRightButtonDisabled()}
            />
        </div>
    );
}
