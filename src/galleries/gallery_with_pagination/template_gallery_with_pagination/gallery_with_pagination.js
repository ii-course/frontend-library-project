import React, { useState } from 'react';
import './gallery_with_pagination.css';
import '../../../cards/events_cards/event_card_with_image/last_card.css';
import '../../../cards/events_cards/event_card_with_image/event_card_with_square_image/event_card_with_square_image.css';
import { PaginationContainer } from '../../../pagination/pagination_container';

export function GalleryWithPagination(props) {
    const defaultCardsPerPage = 40;
    const cardsPerPage = props.cardsPerPage ? props.cardsPerPage : defaultCardsPerPage;
    const [page, setPage] = useState(1);

    // eslint-disable-next-line no-unused-vars
    const getPagesTotal = () => {
        const cardsTotal = props.cards.length;
        return Math.ceil(cardsTotal / cardsPerPage);
    };

    const getCardsForPage = (page) => {
        const { cards } = props;
        const beginIndex = (page - 1) * cardsPerPage;
        const endIndex = beginIndex + cardsPerPage <= cards.length ? beginIndex + cardsPerPage : cards.length;
        return cards.slice(beginIndex, endIndex);
    };

    // eslint-disable-next-line no-unused-vars
    const getFragmentToDisplay = () => {
        const cardsToDisplay = getCardsForPage(page);
        return (
            <div className="cards-container">
                {cardsToDisplay}
            </div>
        );
    };

    return props.cards.length <= 0 ? <div className="nothing-found-message">Nothing found</div> : (
        <div className="gallery-with-pagination">
            <div className="cards-container">
                {props.cards}
            </div>
            <div key="paginationContainer">
                <PaginationContainer
                    style={props.pagination.style}
                    currentPage={page}
                    pagesTotal={props.pagesTotal || 1}
                    onClick={(newPage) => {
                        setPage(newPage);
                        props.setPage(newPage);
                    }}
                />
            </div>
        </div>
    );
}
