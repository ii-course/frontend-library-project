import React from 'react';
import { GalleryWithPagination } from '../../gallery_with_pagination/template_gallery_with_pagination/gallery_with_pagination';
import { FullCommentCardWrapper } from '../../../cards/comments_cards/full_comment_card/full_comment_card';
import { UserReviewCard } from '../../../cards/comments_cards/user_review_card/user_review_card';

export function CommentsGalleryWithPagination(props) {
    const getUserReviewCard = (comment) => (<UserReviewCard comment={comment} />);

    const formCommentsCards = () => {
        const { comments } = props;
        const cards = [];
        comments.forEach((comment) => {
            let card = <FullCommentCardWrapper comment={comment} />;
            if (props.type && props.type === 'userReviews') {
                card = getUserReviewCard(comment);
            }
            cards.push(card);
        });
        return cards;
    };

    return <GalleryWithPagination cards={formCommentsCards()}
        setPage={props.setPage}
        pagesTotal={props.pagesTotal}
        pagination={{ style: 'dark-link' }} />;
}
