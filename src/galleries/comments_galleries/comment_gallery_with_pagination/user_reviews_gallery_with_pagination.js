import React, { useEffect, useState } from 'react';
import {
    getCommentsByUserId,
    getCommentsByUserIdCount
} from '../../../store/repositories/comments_repository';
import { CommentsGalleryWithPagination } from './comment_gallery_with_pagination';
import {getPagesTotal} from "../../../pagination/pages_calculator";

export function UserReviewsGalleryWithPagination(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [comments, setComments] = useState([]);
    const [page, setPage] = useState(1);
    const [pagesTotal, setPagesTotal] = useState(1);

    const PER_PAGE = 10;

    useEffect(() => {
        getCommentsByUserId(props.userId, page)
            .then(
                (commentsFetched) => {
                    setComments(commentsFetched);
                    return getCommentsByUserIdCount(props.userId);
                },
            )
            .then(
                (count) => {
                    setPagesTotal(getPagesTotal(count, PER_PAGE));
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                });
    }, [props.userId, page]);

    if (error) {
        return (
            <div>
                Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return <CommentsGalleryWithPagination
        type="userReviews"
        comments={comments}
        setPage={(pageNum) => {setPage(pageNum);}}
        pagesTotal={pagesTotal}/>;
}
