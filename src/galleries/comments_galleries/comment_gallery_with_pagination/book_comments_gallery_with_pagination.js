import React, { useEffect, useState } from 'react';
import {getCommentsByBookId, getCommentsByBookIdCount} from '../../../store/repositories/comments_repository';
import { CommentsGalleryWithPagination } from './comment_gallery_with_pagination';
import {getPagesTotal} from "../../../pagination/pages_calculator";
import {connect} from "react-redux";
import {mapStateToProps} from "../../../store/store/map_state_to_props";

export function BookCommentsGalleryWithPagination(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [comments, setComments] = useState([]);
    const [page, setPage] = useState(1);
    const [pagesTotal, setPagesTotal] = useState(1);

    const PER_PAGE = 10;

    useEffect(() => {
        getCommentsByBookId(props.bookId, page)
            .then(
                (commentsFetched) => {
                    setComments(commentsFetched);
                    return getCommentsByBookIdCount(props.bookId);
                },
            )
            .then(
                (count) => {
                    setPagesTotal(getPagesTotal(count, PER_PAGE));
                    setIsLoaded(true);
                },
                (error) => {
                    setError(error);
                    setIsLoaded(true);
                });
    }, [props.bookId, page, props.addedComments]);

    if (error) {
        return (
            <div>
            Error:{error.message}
            </div>
        );
    } if (!isLoaded) {
        return <div>Loading...</div>;
    }
    return comments.length <= 0 ? <div className="nothing-found-message">No comments found.</div> :
        <CommentsGalleryWithPagination comments={comments}
            setPage={(pageNum) => {setPage(pageNum);}}
            pagesTotal={pagesTotal}/>;
}

export default connect(mapStateToProps)(BookCommentsGalleryWithPagination);
