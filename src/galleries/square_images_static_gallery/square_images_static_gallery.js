import React from 'react';
import './square_images_static_gallery.css';

export function SquareImagesStaticGallery(props) {
    const elements = [];
    props.images.forEach((img) => {
        const element = <img src={img.src} alt={img.alt} className="square-img" />;
        elements.push(element);
    });
    return (
        <div className="square-img-gallery">
            {elements}
        </div>
    );
}
