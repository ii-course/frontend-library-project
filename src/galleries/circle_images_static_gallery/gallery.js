import React from 'react';
import { CircleImageCard } from '../../cards/circle_image_card/card';
import './gallery.css';
import '../../scss/wrapper.css';

export function CircleImagesStaticGallery(props) {
    const cards = [];
    props.cards.forEach((card) => {
        const cardElement = (
            <CircleImageCard
                img={card.img} heading={card.heading} text={card.text}
                key={card.id}
            />
        );
        cards.push(cardElement);
    });
    return (
        <div className="circle-image-cards-gallery content-wrapper">
            {cards}
        </div>
    );
}
