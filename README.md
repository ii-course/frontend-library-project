<a name="readme-top"></a>

# Frontend Library Project

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

A web application for a library. Developed with ReactJS. All designs are unique. 

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

```sh
npm install npm@latest -g
```

### Installation

1. Clone the repo

```sh
git clone https://gitlab.com/ii-course/frontend-library-project.git
```
2. Install Yarn packages

```sh
yarn install
```
3. Run React application
```sh
yarn start
```
4. Run mock server
```sh
yarn start:mock
```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->
## Usage

After the application is set up and running, navigate to http://localhost:3000/. You should be able to use the app.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTACT -->
## Contact

Project Link: [https://gitlab.com/ii-course/frontend-library-project](https://gitlab.com/ii-course/frontend-library-project)

<p align="right">(<a href="#readme-top">back to top</a>)</p>
